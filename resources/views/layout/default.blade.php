<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
	<script>
		var local = {
			base: "{{ url('/') }}"
		} 
	</script>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div style="background: #000000b0;height: 100%;width: 100%;position: fixed;z-index: 9999;display:none" class="loading">
		<h3 style="color:#fff;text-align:center;margin-top:25%">CARGANDO...</h3>
	</div>
	<div class="wrapper">
		<header class="main-header">
			@include('includes.header')
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			@include('includes.menu')
		</aside>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			@yield('content')
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			@include('includes.footer')
		</footer>
		<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	
	@include('includes.script')
</body>
</html>