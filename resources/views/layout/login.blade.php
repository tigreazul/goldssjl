<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
	<script>
		var local = {
			base: "{{ url('/') }}"
		} 
	</script>
</head>
<div style="top:0;background: #000000b0;height: 100%;width: 100%;position: absolute;z-index: 9999;display:none" class="loading">
    <h3 style="color:#fff;text-align:center;margin-top:30%">Validando información...</h3>
</div>
<body class="hold-transition login-page">
    <div class="login-box">
        
        @yield('content')
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    @include('includes.script')
</body>
</html>
