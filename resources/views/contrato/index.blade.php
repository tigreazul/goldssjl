@extends('layout.default')
@section('content')
    <section class="content-header" style="margin-bottom: 20px;">
        <h1>
            Contrato
            <small>Listado</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Buscar</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-4">
                            <label>Fecha Inicio</label>
                            <input type="date" class="form-control" name="finicio" id="finicio">
                        </div>
                        <div class="col-xs-4">
                            <label>Fecha Fin</label>
                            <input type="date" class="form-control" name="ffin" id="ffin">
                        </div>
                        <div class="col-xs-4">
                            <label>Usuario</label>
                            <input type="text" class="form-control" name="usuario" id="usuario">
                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-danger pull-right">Buscar</button>
                    </div>
                    <hr>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Registro</h3>
                    </div>
                    <div class="box-body">
                        <table id="lstContrato" class="table hover table-bordered table-striped display" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Acuerdo</th>
                                    <th>Membresia</th>
                                    <th>Usuario</th>
                                    <th>Estado</th>
                                    <th>Impresión</th>
                                </tr>
                            </thead>
                            <tbody class="data-prospecto">
                                @foreach($acuerdo as $acuerds)
                                    <tr>
                                        <td>{{ $acuerds->Acuerdo }}</td>
                                        <td>{{ $acuerds->membresia }}</td>
                                        <td>{{ $acuerds->Usuario }}</td>
                                        <td>{{ $acuerds->estadoocu }}</td>
                                        <td>
                                            <a href="#" data-imp="{{ $acuerds->idacuerdo }}" class="btn btn-success"><i class="fa fa-print"></i> Imprimir</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Acuerdo</th>
                                    <th>Membresia</th>
                                    <th>Usuario</th>
                                    <th>Estado</th>
                                    <th>Impresión</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
@stop
