@extends('layout.default')
@section('content')

    <section class="content-header" style="margin-bottom: 20px;">
        <h1>
            Calendario
            <small>registro</small>
        </h1>
    </section>

    <div class="col-sm-12">
        @if (Session::has('success'))
            @php
                $class 		= session('success') ? 'alert-success': (session('error') ? 'alert-danger': (session('warning') ? 'alert-warning': ''));
                $class_icon = session('success') ? 'check': (session('error') ? 'exclamation-circle': (session('warning') ? 'exclamation-triangle': ''));
                $message 	= session('success') ? session('success'): (session('error') ? session('error'): (session('warning') ? session('warning'): 'No hay mensaje'));
            @endphp
            <div class="alert {!! $class !!} text-center">  
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
                <p><span class="fa fa-{!! $class_icon !!}"></span> {!! $message !!}</p>   
            </div>
        @endif
        @if ($errors->any())
            <div class="alert text-center">
                <h6>Por favor corrige los errores debajo:</h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div> 
        @endif
    </div>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Calendario</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Listado</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box box-primary">
                                <div class="box-body no-padding">
                                    <!-- THE CALENDAR -->
                                    <div id="calendar"></div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane " id="tab_2">
                            <h4>Lista de eventos</h4>
                            <table class="table table-bordered table-hover" id="tbl-main">
                                <thead> 
                                    <tr>
                                        <th class="vhead" style="width: 10px">#</th>
                                        <th class="vhead">Titulo</th>
                                        <th class="vhead">Fecha</th>
                                        <th class="vhead">Fecha Creación</th>
                                        <th class="vhead">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="5" align="center">
                                            @foreach ($calendario as $cal)
                                                <tr>
                                                    <td>{{ $cal->ID }}</td>
                                                    <td>{{ $cal->TITULO }}</td>
                                                    <td>{{ \Carbon\Carbon::parse($cal->FECHA_INICIO)->format('Y-m-d')."-".$cal->HORA_INICIO }}</td>
                                                    <td>{{ \Carbon\Carbon::parse($cal->FECHA_FIN)->format('Y-m-d')."-".$cal->HORA_FIN }}</td>
                                                    <td><a href="javascript:void(0)" class="btn btn-danger delete-event-btn" data-id="{{ $cal->ID }}" >Eliminar</a></td>
                                                </tr>
                                            @endforeach
                                            {{-- <p><strong>NO EXISTEN DATOS </strong></p> --}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    @include('calendar.script')
    @include('calendar.modal')
@stop
