<div class="modal fade" id="modalHoy" aria-labelledby="myModalLabel" aria-hidden="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modal-title"></h4>
            </div>

            <form action="{{ route('calendar.registro') }}" class="form" method="POST" id="calendar_data" accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Cargo:</label>
                        <select name="cargo" id="cargo" class="form-control" required="required">
                            <option value="1">SUPERINTENDENTE</option>
                            <option value="2">GERENTE GENERAL</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Titulo(*):</label>
                        <input type="text" name="titulo" id="titulo" class="form-control" required="required">
                        <input type="hidden" id="id" name="id">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-md-2 nopadding control-label">Fecha Inicio:</label>
                        <div class="col-md-4 nopadding">
                            <input type="date" name="iniDate" id="iniDate" class="form-control" required="required">
                        </div>
                        
                        <label for="recipient-name" class="col-md-2 control-label">Hora:</label>
                        <div class="col-md-4 nopadding">
                            <input type="time" name="iniHora" id="iniHora" class="form-control times" required="required">
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-md-2 nopadding control-label">Fecha Fin:</label>
                        <div class="col-md-4 nopadding">
                            <input type="date" name="finDate" id="finDate" class="form-control" required="required">
                        </div>
                        
                        <label for="recipient-name" class="col-md-2 control-label">Hora:</label>
                        <div class="col-md-4 nopadding">
                            <input type="time" name="finTime" id="finTime" class="form-control times" required="required">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Descripción:</label>
                        <textarea class="form-control" name="description" id="description" required="required"></textarea>
                    </div>
                </div>
                <div class="modal-footer" id="modal-footer">
                    <input type="hidden" name="id">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modalDescripcion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title header-title" id="myModalLabel" style="font-weight: bold !important"></h4>
            </div>
            <div class="modal-body">
                <table class="table">
                    <tr>
                        <td class="vhead">Fecha Inicio</td>
                        <td class="vhead">Fecha Fin</td>
                    </tr>
                    <tr>
                        <td class="content-table"><div class="iniDate"></div></td>
                        <td class="content-table"><div class="finDate"></div></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td class="vhead">Hora Inicio</td>
                        <td class="vhead">Hora Fin</td>
                    </tr>
                    <tr>
                        <td class="content-table"><div class="iniHora"></div></td>
                        <td class="content-table"><div class="finTime"></div></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td class="vhead" colspan="2">Descripcion</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="content-table"><div class="descripcion"></div></td>
                    </tr>
                </table>

                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<style>
    .vhead {
        background: #505759;
        color: #fff;
        text-align: left;
        font-weight: bold;
        border: 1px solid #505759;
        text-transform: uppercase;
    }
    .content-table{
        border: 1px solid #505759;
    }
    .fc-content{
        padding: 5px !important;
    }
</style>