<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Calendario</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">

        <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
        {{-- Calendario --}}
        <script src="{{ asset('plugins/jQuery/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>

        <script src="{{ asset('plugins/jQuery/moment.min.js') }}"></script>
        <script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
        <script src="{{ asset('plugins/fullcalendar/lang-all.js') }}"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
            var app = { 
                base : "{{ url('')}}"
            }; 
        </script>
    </head>
    <body>
        <div id="calendar"></div>

        <script>
            $(function () {
                function ini_events(ele) {
                    ele.each(function () {
                        var eventObject = {
                            title: $.trim($(this).text())
                        };
                        $(this).data('eventObject', eventObject);
                        $(this).draggable({
                            zIndex: 1070,
                            revert: true,
                            revertDuration: 0
                        });
                    });
                }
                ini_events($('#external-events div.external-event'));
                var date = new Date();
                var d = date.getDate(),m = date.getMonth(),y = date.getFullYear();
                
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    lang: 'es',
                    events: app.base+'/calendar-lista',
                    editable: false,
                    droppable: false,
                    eventClick: function(calEvent, jsEvent, view) {
                        currentEvent = calEvent;
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url     : app.base+'/calendar-data',
                            type    : 'POST',
                            dataType: 'JSON',
                            data    : "id="+ currentEvent._id,
                            success : function(data){  
                                modal({
                                    title       : data.titulo,
                                    description : data.description,
                                    fin         : data.fecha_fin,
                                    inic        : data.fecha_inicio,
                                    finTime     : data.hora_fin,
                                    iniHora     : data.hora_inicio,
                                    titulo      : data.titulo,
                                    event       : calEvent
                                });
                            },
                            error   : function(jqxhr, textStatus, error){
                                // Muestra mensaje
                                // $btnNuevaCapacitacion.show_message({ text: jqxhr.responseText});
                            }
                        });
                    }
                });

                function toYYYYMMDD(d,separador,formato) {
                    var yyyy = d.getFullYear().toString();
                    var mm = (d.getMonth() + 101).toString().slice(-2);
                    var dd = (d.getDate() + 100).toString().slice(-2);
                    if(formato == 1){
                        return  yyyy + separador + mm + separador + dd;
                    }else{
                        return  dd + separador + mm + separador + yyyy;
                    }
                }

                //MODAL
                function modal(data) {
                    $('.modal-title').html(data.title);
                    $('#modal-footer button:not(".btn-default")').remove();
                    $('.title').val(data.event ? data.titulo : '');
                    $('.titulo').val(data.event ? data.titulo : '');
                    if( ! data.event) {
                        var now = new Date();
                        var time = now.getHours() + ':' + (now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes());
                    }
                    $('.descripcion').html(data.event ? data.description : '');
                    $('.iniDate').html(data.inic);
                    $('.finDate').html(data.fin);
                    if(data.event){
                        $('.iniHora').html(data.event ? data.iniHora : '');
                        $('.finTime').html(data.event ? data.finTime : '');
                    }else{
                        $('.times').val(time);
                    }
                    $('#modalDescripcion').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            }); 
        </script>

        @include('calendar.modal')
    </body>
</html>