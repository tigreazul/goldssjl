<script>
    $(function () {
        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
            ele.each(function () {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });
            });
        }
        var $mo_hoy = modal_helper('#modalHoy');
        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear();
        
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            lang: 'es',
            events: app.base+'/calendar-lista/',
            editable: false,
            droppable: false,
            selectable: true,
            // selectHelper: true,
            select: function(start, end) {
                $mo_hoy.set_action('nuevas_eventos');
                modal({
                    buttons: {
                        add: {
                            id: 'add-event',
                            css: 'btn-success',
                            label: 'Agregar'
                        }
                    },
                    inic: start.format('YYYY-MM-DD'),
                    fin: toYYYYMMDD(end._d,'-',1),
                    title: 'Agregar Evento (' + start.format('YYYY-MM-DD') + ' - '+ toYYYYMMDD(end._d,'/',0) + ')'
                });
                $('#calendar').fullCalendar('unselect');
            },
            drop: function (date, allDay) {
                var originalEventObject = $(this).data('eventObject');
                var copiedEventObject = $.extend({}, originalEventObject);
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.backgroundColor = $(this).css("background-color");
                copiedEventObject.borderColor = $(this).css("border-color");

                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },            
            
            eventDrop: function(event, delta, revertFunc) {  
                $.post(app.base+'calendar/dragUpdateEvent',{                            
                id:event.id,
                date: event.start.format()
                }, function(result){
                    if(result)
                    {
                        alert('Updated');
                    }
                    else
                    {
                        alert('Try Again later!')
                    }
                });
            },

            eventClick: function(calEvent, jsEvent, view) {
                
                currentEvent = calEvent;
                $mo_hoy.set_action('modifica_evento');
                $mo_hoy.form.find('input[name="id"]').val(currentEvent._id);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url     : app.base+'/calendar-data',  // URL,
                    type    : 'POST',
                    dataType: 'JSON',
                    data    : "id="+ currentEvent._id,
                    success : function(data){  
                        modal({
                            buttons: {
                                delete: {
                                    id: 'delete-event',
                                    css: 'btn-danger btn-row-delete',
                                    label: 'Eliminar'
                                },
                                update: {
                                    id: 'update-event',
                                    css: 'btn-success btn-row-update',
                                    label: 'Actualizar'
                                }
                            },
                            title: 'Editar Evento "' + data.titulo + '"',
                            description: data.description,
                            fin: data.fecha_fin,
                            inic: data.fecha_inicio,
                            finTime: data.hora_fin,
                            iniHora: data.hora_inicio,
                            titulo: data.titulo,
                            event: calEvent,
                            cargo: data.cargo,
                            color: data.color,
                            id:data.id
                        });
                    },
                    error   : function(jqxhr, textStatus, error){
                        // Muestra mensaje
                        // $btnNuevaCapacitacion.show_message({ text: jqxhr.responseText});
                    }
                });

            }

        });

    
        function toYYYYMMDD(d,separador,formato) {
            var yyyy = d.getFullYear().toString();
            var mm = (d.getMonth() + 101).toString().slice(-2);
            var dd = (d.getDate() + 100).toString().slice(-2);
            if(formato == 1){
                return  yyyy + separador + mm + separador + dd;
            }else{
                return  dd + separador + mm + separador + yyyy;
            }
        }

        //MODAL
        function modal(data) {
            console.log(data);
            $('#modal-title').html(data.title);
            $('#modal-footer button:not(".btn-default")').remove();
            $('#modal-footer a:not(".btn-default")').remove();
            $('#title').val(data.event ? data.titulo : '');
            $('#titulo').val(data.event ? data.titulo : '');
            if( ! data.event) {
                var now = new Date();
                var time = now.getHours() + ':' + (now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes());
            }
            $('#description').val(data.event ? data.description : '');
            $('#iniDate').val(data.inic);
            $('#finDate').val(data.fin);
            $('#cargo').val(data.cargo);
            if(data.event){
                $('#iniHora').val(data.event ? data.iniHora : '');
                $('#finTime').val(data.event ? data.finTime : '');
            }else{
                $('.times').val(time);
            }
            $.each(data.buttons, function(index, button){
                if(button.id == 'delete-event' || button.id == 'update-event'){
                    $('#modal-footer').prepend('<a href="javascript:void(0)" id="' + button.id  + '" class="btn ' + button.css + '">' + button.label + '</a>')
                }else{
                    $('#modal-footer').prepend('<button type="submit" id="' + button.id  + '" class="btn ' + button.css + '">' + button.label + '</button>')
                }
            });
            $('#modalHoy').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#id').val(data.id);
        }

        // Handle Click on Delete Button
        $('.modal').on('click', '#delete-event',  function(e){
            // $mo_hoy.set_action('delete_evento');
            $mo_hoy.form.find('input[name="id"]').val(currentEvent._id);
            // alert(currentEvent._id);
            // $.post(app.base+'calendar-delete', + currentEvent._id, function(result){
            //     $('.modal').modal('hide');
            //     $('#calendar').fullCalendar("refetchEvents");
            // });

            $.post( app.base+'/calendar-delete', { d: currentEvent._id },function( data ) {
                $('.modal').modal('hide');
                // $('#calendar').fullCalendar("refetchEvents");
                location.reload();
            }, "json");
        });

        $('body').on('click', '.delete-event-btn',  function(e){
            let vId = $(this).data('id');
            // alert(vId);
            if(confirm('Desea eliminar el evento?') == true){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url     : app.base+'/calendar-delete',
                    type    : 'POST',
                    dataType: 'JSON',
                    data    : { d: vId },
                    success : function(data){  
                        location.reload();    
                    },
                    error   : function(jqxhr, textStatus, error){
                        // Muestra mensaje
                        // $btnNuevaCapacitacion.show_message({ text: jqxhr.responseText});
                    }
                });
            }
        });

        $('.modal').on('click', '#update-event',  function(e){
            $mo_hoy.form.find('input[name="id"]').val(currentEvent._id);

            let form = $('#calendar_data').serialize();
            console.log(form); 
            // return false;
            $.ajax({
                url     : app.base+'/calendar-update/'+currentEvent._id,  // URL,
                type    : 'POST',
                dataType: 'JSON',
                data    : form,
                success : function(data){  
                    $('.modal').modal('hide');
                    location.reload();
                },
                error   : function(jqxhr, textStatus, error){
                    // Muestra mensaje
                    // $btnNuevaCapacitacion.show_message({ text: jqxhr.responseText});
                }
            });
        });

    });
</script>