@extends('layout.default')
@section('content')

    <section class="content-header" style="margin-bottom: 20px;">
        <h1>
            Prospecto
            <small>registro</small>
        </h1>
    </section>

    <div class="col-sm-12">
        @if (Session::has('success'))
            @php
                $class 		= session('success') ? 'alert-success': (session('error') ? 'alert-danger': (session('warning') ? 'alert-warning': ''));
                $class_icon = session('success') ? 'check': (session('error') ? 'exclamation-circle': (session('warning') ? 'exclamation-triangle': ''));
                $message 	= session('success') ? session('success'): (session('error') ? session('error'): (session('warning') ? session('warning'): 'No hay mensaje'));
            @endphp
            <div class="alert {!! $class !!} text-center">  
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
                <p><span class="fa fa-{!! $class_icon !!}"></span> {!! $message !!}</p>   
            </div>
        @endif
        @if ($errors->any())
            <div class="alert text-center">
                <h6>Por favor corrige los errores debajo:</h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div> 
        @endif
    </div>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Prospecto</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Seguimiento</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box box-primary">
                                <div class="box-body no-padding">
                                    <form action="{{ route('prospecto.registro') }}" class="form" method="POST" id="formProspecto" accept-charset="UTF-8" >
                                        {{ csrf_field() }}
                                        <div class="modal-body">
                                            <div class="form-group has-success">
                                                <div class="col-md-2">
                                                    <label for="recipient-name" class="control-label">Codigo Procedencia:</label>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="input-group ">
                                                        <input type="text" name="prospecto" id="prospecto" maxlength="5" class="form-control" required="required">
                                                        <input type="hidden" name="persoid" id="persoid">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target="#modalProcedencia"><i class="fa fa-search"></i></button>
                                                        </span>
                                                    </div>
                                                    <span class="help-block"><strong id="texto_cliente">NUEVO</strong></span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Procedencia:</label>
                                                    <select class="form-control" name="procedencia" id="procedencia" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($procedencia as $proc)
                                                            <option value="{{ $proc->idProcedencia }}">{{ $proc->procedencia }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Sub Procedencia:</label>
                                                    <select class="form-control" name="subprocedencia" id="subprocedencia" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($subprocedencia as $sub)
                                                            <option value="{{ $sub->IdSubProcedencia }}">{{ $sub->SubProcedencia }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Corporativo:</label>
                                                    <select class="form-control" name="corporativo" id="corporativo" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($corporativo as $corp)
                                                            <option value="{{ $corp->codigo }}">{{ trim($corp->nombre) }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Campaña:</label>
                                                    <select class="form-control" name="campania" id="campania" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($campania as $camp)
                                                            <option value="{{ $camp->IdCampana }}">{{ $camp->Descripcion }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Nº Hoja de Atención:</label>
                                                    <input type="text" name="nro_hoja" id="nro_hoja_atencion" class="form-control times" required="required">
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Fitness Consultant:</label>
                                                    <select class="form-control" name="fitness" id="fitness" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($fitness as $fit)
                                                            <option value="{{ $fit->TITULO }}">{{ $fit->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                            </div>


                                            <h4 class="page-header">Datos Personales</h4>

                                            <div class="form-group has-success">
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Documento:</label>
                                                    <select class="form-control" name="documento" id="tipo_documento" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($tdocument as $tdoc)
                                                            <option value="{{ $tdoc->idtipo_documento }}">{{ $tdoc->nombre }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <div class="input-group">
                                                        <label for="recipient-name" class="nopadding control-label">Nº Documento:</label>
                                                        <input type="text" name="nro_documento" id="num_documento" maxlength="11" class="form-control" required="required">
                                                        <span class="input-group-btn" id="validDNI" style="padding-top: 25px;">
                                                            <a href="#" class="btn btn-success btn-flat"><i class="fa fa-search"></i> Buscar DNI</a>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <div class="input-group">
                                                        <label for="recipient-name" class="control-label">Distrito:</label>
                                                        <input type="text" name="textUbigeo" id="textUbigeo" class="form-control" required="required" disabled>

                                                        <input type="hidden" name="distrito" id="distrito" class="form-control" required="required">
                                                        <input type="hidden" name="provincia" id="provincia" class="form-control" required="required">
                                                        <input type="hidden" name="departamento" id="departamento" class="form-control" required="required">
                                                        
                                                        <span class="input-group-btn" style="padding-top: 25px;">
                                                            <a href="#" class="btn btn-success btn-flat" id="btn-ubigeo" data-toggle="modal" data-target="#modalUbigeo"><i class="fa fa-plus-square"></i> Agregar</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Nombre:</label>
                                                    <input type="text" name="nombre" id="nombre" class="form-control" required="required">
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Apellido Paterno:</label>
                                                    <input type="text" name="ap_paterno" id="apellido_paterno" class="form-control" required="required">
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Apellido Materno:</label>
                                                    <input type="text" name="ap_materno" id="apellido_materno" class="form-control" required="required">
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">
                                                <div class="col-md-12 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Dirección:</label>
                                                    <input type="text" name="direccion" id="direccion" class="form-control" required="required">
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Fecha Nacimiento:</label>
                                                    <input type="date" name="fnacimiento" id="fecha_nacimiento" class="form-control" required="required">
                                                </div>
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Sexo:</label>
                                                    <select class="form-control" name="sexo" id="sexo" required>
                                                        <option value="0">[SELECCIONE]</option>
                                                        <option value="1">MASCULINO</option>
                                                        <option value="2">FEMENINO</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Estado Civil:</label>
                                                    <select class="form-control" name="ecivil" id="estado_civil" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($ecivil as $civil)
                                                            <option value="{{ $civil->idEstadoCivil }}">{{ $civil->Descripcion }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Telefono:</label>
                                                    <input type="text" name="telefono" id="telefono" class="form-control" required="required">
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Email:</label>
                                                    <input type="text" name="email" id="email" class="form-control" required="required">
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Parentesco:</label>
                                                    <select class="form-control" name="parentesco" id="parentesco" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($parentesco as $parent)
                                                            <option value="{{ $parent->idParentesco }}">{{ $parent->Descripcion }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>


                                            <div class="form-group has-success">

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Ocupación:</label>
                                                    <select class="form-control" name="ocupacion" id="ocupacion" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($ocupacion as $ocupa)
                                                            <option value="{{ $ocupa->idOcupacion }}">{{ $ocupa->Descripcion }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Hijos:</label>
                                                    <select class="form-control" name="hijos" id="hijos" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        <option value="Si">SI</option>
                                                        <option value="No">NO</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                        <label for="recipient-name" class="control-label">Nacionalidad:</label>
                                                    <select class="form-control" name="nacionalidad" id="nacionalidad" required>
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($nacionalidad as $nac)
                                                            <option value="{{ $nac->idPais }}">{{ $nac->Descripcion }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">

                                                <div class="col-md-12 nopadding">
                                                    <label for="recipient-name" class="control-label">Membresia:</label>
                                                    <select class="form-control" name="membresia" id="membresia" >
                                                        <option value="">[SELECCIONE]</option>
                                                        @foreach($membresia as $memb)
                                                            <option value="{{ $memb->idarticulo }}">{{ $memb->descripcion }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>


                                        </div>

                                        <div class="modal-footer" id="modal-footer">
                                            <input type="hidden" name="id">
                                            <button type="submit" class="btn btn-success" id="nuevo" style="display: none;"><i class="fa fa-newspaper-o"></i> Nuevo</button>
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Grabar</button>
                                            <a class="btn btn-info" id="tranferir-data" style="display: none;"><i class="fa fa-share"></i> Transferir</a>
                                            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane " id="tab_2">
                            
                            <a href="#" class="btn btn-primary" style="margin-bottom: 10px" data-toggle="modal" data-target="#modalNota"><i class="fa fa-book"></i> NUEVA NOTA</a>

                            <div class="box box-primary">
                                <div class="box-body no-padding" style="margin-top: 20px;">
                                    <table id="example2" class="table table-bordered table-striped" style="padding-top: 20px;">
                                        <thead>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Asunto</th>
                                                <th>Información</th>
                                                <th>Fecha Seguimiento</th>
                                                <th>Fecha Registro</th>
                                                <th>Usuario Creación</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lstNota">
                                            @foreach($notas as $nota)
                                            <tr>
                                                <td>{{ $nota->asunto }}</td>
                                                <td>{{ $nota->asunto }}</td>
                                                <td>{{ $nota->informacion }}</td>
                                                <td>{{ $nota->fecha_seguimiento }}</td>
                                                <td>{{ $nota->fecha_registro }}</td>
                                                <td>{{ $nota->usuario_creacion }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Asunto</th>
                                                <th>Información</th>
                                                <th>Fecha Seguimiento</th>
                                                <th>Fecha Registro</th>
                                                <th>Usuario Creación</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    @include('prospecto.script')
    
    @include('prospecto.modal')
@stop
