<script>
    $(function () {
        $('.datepicker').datepicker();
        $('#prospecto').focus();
        $('#example2').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : true,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
        });

        // $('#lstprospecto').DataTable({
        //       'paging'      : true,
        //       'lengthChange': false,
        //       'searching'   : true,
        //       'ordering'    : true,
        //       'info'        : true,
        //       'autoWidth'   : false
        // })

        $('#lstprospecto').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            "bProcessing": true,
            "serverSide": true,
            "ajax":{
                url :app.base+'/data-prospecto/', // json datasource
                type: "post",  // type of method  , by default would be get
                error: function(){  // error handling code
                    $("#lstprospecto_processing").css("display","none");
                }
            }
        });


        $('.selProspecto').click(function(e){
            e.preventDefault();
        });

        $("body").keydown(function(event) {
            if(event.which == 113) { //F2
                $("#modalProcedencia").modal("show");
                return false;
            }
            if(event.which == 114) { //F3
                $(".loading").show();
                setTimeout(function(){
                    $(".loading").hide();
                    nuevoProspecto();
                }, 1500);
                return false;
            }
        });

        $(document).on('change','#departamentos',function(e){
            e.preventDefault();
            let code = $(this).find(':selected').data('id');
            console.log(code); 
            // return false;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url     : app.base+'/provincia/'+code,
                type    : 'GET',
                dataType: 'JSON',
                data    : {},
                success : function(data){
                    $("#provincias").removeAttr('disabled');
                    $('#provincias').html("");
                    let vhtml = "";
                    $( data.data ).each(function( index, element ){
                        vhtml += '<option value="'+element.descripcion+'" data-id="'+element.provID+'">'+element.descripcion+'</option>'
                    });
                    $('#provincias').html(vhtml);
                    $('#distritos').html('<option value="">[SELECCIONE]</option>');
                },
                error   : function(jqxhr, textStatus, error){
                    console.log(jqxhr.responseText);
                }
            });
        });

        $(document).on('change','#provincias',function(e){
            e.preventDefault();
            let code = $(this).find(':selected').data('id');
            console.log(code); 
            // return false;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url     : app.base+'/distrito/'+code,
                type    : 'GET',
                dataType: 'JSON',
                data    : {},
                success : function(data){
                    $("#distritos").removeAttr('disabled');
                    $('#distritos').html("");
                    let vhtml = "";
                    $( data.data ).each(function( index, element ){
                        vhtml += '<option value="'+element.descripcion+'" data-id="'+element.distID+'">'+element.descripcion+'</option>'
                    });
                    $('#distritos').html(vhtml);
                },
                error   : function(jqxhr, textStatus, error){
                    console.log(jqxhr.responseText);
                }
            });
        });
        
        $('#addUbigeo').click(function(e){
            e.preventDefault();
            let dep  = $('#departamentos').val();
            let prov = $('#provincias').val();
            let dist = $('#distritos').val();
            
            $("#distrito").val(dep);
            $("#provincia").val(prov);
            $("#departamento").val(dist);
            
            $(".loading").show();
            setTimeout(function(){
                if(dist == '' || dist == null){
                    swal("Debe de seleccionar el ubigeo");
                    $("#textUbigeo").val("");
                }else{
                    $("#textUbigeo").val(dep+" - "+prov+" - "+dist);
                    $("#modalUbigeo").modal("hide");
                }
                $(".loading").hide();
            }, 1500);

        });

        $(document).on('click','.seleccion',function(e){
            e.preventDefault();
            let id = $(this).data('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url     : app.base+'/getconsulta/'+id,
                type    : 'GET',
                dataType: 'JSON',
                data    : {},
                beforeSend : function(data){
                    $(".loading").show();
                },
                success : function(data){
                    $(".loading").hide();
                    console.log(data.data);
                    $('#persoid').val(data.data.C_Cliente);
                    $("#apellido_materno").val(data.data.ApelMat);
                    $("#apellido_paterno").val(data.data.Apelpat);
                    $("#campania").val(data.data.IdCampana);
                    $("#prospecto").val(data.data.Id_Guest);
                    $("#direccion").val(data.data.Direccion);
                    
                    $("#corporativo").val(data.data.corporativo);


                    $("#departamento").val(data.data.direccion_departamento);
                    $("#distrito").val(data.data.direccion_distrito);
                    $("#provincia").val(data.data.direccion_provincia);
                    $("#textUbigeo").val(data.data.Descripcion_Ubigeo);
                    
                    $("#email").val(data.data.Email);
                    $("#estado_civil").val(data.data.C_EstadoCivil);
                    $("#fecha_nacimiento").val(data.data.Fecha_Nacimiento);
                    
                    $("#fitness").val(data.data.Fitness_Consultant_Ult);
                    $("#hijos").val(data.data.Hijos);
                    
                    $("#idpersona").val(data.data.idpersona);

                    $("#nacionalidad").val(data.data.C_Pais);
                    $("#nombre").val(data.data.Nombres);
                    $("#nro_hoja_atencion").val(data.data.NroCartilla);
                    $("#num_documento").val(data.data.DNI);
                    $("#ocupacion").val(data.data.Ocupacion);
                    $("#parentesco").val(data.data.C_TipoParentesco);

                    $("#procedencia").val(data.data.IdProcedenciaULT);
                    $("#subprocedencia").val(data.data.IdSubProcedenciaULT);
                    
                    $("#sexo").val(data.data.Sexo);
                    $("#telefono").val(data.data.Telefono01);
                    $("#tipo_documento").val(data.data.TipoDocumento);
                    $('#membresia').val(data.data.IdMembresia);

                    $('#btn-ubigeo').attr('disabled','disabled');
                    $('#btn-ubigeo').removeAttr('data-target');

                    $("#apellido_materno").attr('disabled','disabled');
                    $("#apellido_paterno").attr('disabled','disabled');
                    $("#campania").attr('disabled','disabled');
                    $("#corporativo").attr('disabled','disabled');
                    $("#direccion").attr('disabled','disabled');
                    $("#email").attr('disabled','disabled');
                    $("#estado_civil").attr('disabled','disabled');
                    $("#fecha_nacimiento").attr('disabled','disabled');
                    $("#fitness").attr('disabled','disabled');
                    $("#hijos").attr('disabled','disabled');
                    $("#nacionalidad").attr('disabled','disabled');
                    $("#nombre").attr('disabled','disabled');
                    $("#nro_hoja_atencion").attr('disabled','disabled');
                    $("#num_documento").attr('disabled','disabled');
                    $("#ocupacion").attr('disabled','disabled');
                    $("#parentesco").attr('disabled','disabled');
                    $("#procedencia").attr('disabled','disabled');
                    $("#sexo").attr('disabled','disabled');
                    $("#subprocedencia").attr('disabled','disabled');
                    $("#telefono").attr('disabled','disabled');
                    $("#tipo_documento").attr('disabled','disabled');
                    $('#nuevo').show();

                    if(data.data.C_Cliente !=''){
                        $('#texto_cliente').text('TRANSFERIDO');
                    }else{
                        $('#texto_cliente').text('POR TRANSFERIR');
                    }
                    
                    $("#modalProcedencia").modal("hide");
                    
                    $('#tranferir-data').show();
                },
                error   : function(jqxhr, textStatus, error){
                    console.log(jqxhr.responseText);
                }
            });
        });

        $('#nuevo').click(function(e){
            e.preventDefault();
            $(this).hide();
            $(".loading").show();
            // console.log(data.data);
            setTimeout(function(){
                $(".loading").hide();
                nuevoProspecto();
            }, 1500);
        });

        function nuevoProspecto(){
            $('#persoid').val("");
            $("#apellido_materno").val("");
            $("#apellido_paterno").val("");
            $("#campania").val("");
            $("#prospecto").val("");
            $("#corporativo").val("");
            $("#direccion").val("");
            $("#departamento").val("");
            $("#distrito").val("");
            $("#provincia").val("");
            $("#textUbigeo").val("");
            $("#email").val("");
            $("#estado_civil").val("");
            $("#fecha_nacimiento").val("");
            $("#fitness").val("");
            $("#hijos").val("");
            $("#idpersona").val("");
            $("#nacionalidad").val("");
            $("#nombre").val("");
            $("#nro_hoja_atencion").val("");
            $("#num_documento").val("");
            $("#ocupacion").val("");
            $("#parentesco").val("");
            $("#procedencia").val("");
            $("#sexo").val("");
            $("#subprocedencia").val("");
            $("#telefono").val("");
            $("#tipo_documento").val("");
            $('#btn-ubigeo').attr('data-target','#modalUbigeo');
            $('#btn-ubigeo').removeAttr('disabled');
            $("#apellido_materno").removeAttr('disabled');
            $("#apellido_paterno").removeAttr('disabled');
            $("#campania").removeAttr('disabled');
            $("#corporativo").removeAttr('disabled');
            $("#direccion").removeAttr('disabled');
            $("#email").removeAttr('disabled');
            $("#estado_civil").removeAttr('disabled');
            $("#fecha_nacimiento").removeAttr('disabled');
            $("#fitness").removeAttr('disabled');
            $("#hijos").removeAttr('disabled');
            $("#nacionalidad").removeAttr('disabled');
            $("#nombre").removeAttr('disabled');
            $("#nro_hoja_atencion").removeAttr('disabled');
            $("#num_documento").removeAttr('disabled');
            $("#ocupacion").removeAttr('disabled');
            $("#parentesco").removeAttr('disabled');
            $("#procedencia").removeAttr('disabled');
            $("#sexo").removeAttr('disabled');
            $("#subprocedencia").removeAttr('disabled');
            $("#telefono").removeAttr('disabled');
            $("#tipo_documento").removeAttr('disabled');
            $('#texto_cliente').text('NUEVO');
        }

        $("#validDNI").click(function(e){
            e.preventDefault();
            let dni = $("#num_documento").val();
            // console.log(dni.length); return false;
            if(dni == '' || dni.length < 8){
                swal("Debe ingresar DNI correctamente");
            }else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url     : app.base+'/valid-documento/'+dni,
                    type    : 'GET',
                    dataType: 'JSON',
                    data    : {},
                    beforeSend : function(data){
                        $(".loading").show();
                    },
                    success : function(data){
                        $(".loading").hide();
                        if(data.status == 'ok'){
                            console.log(data.status);
                            $("#apellido_materno").val(data.data.Apelpat);
                            $("#apellido_paterno").val(data.data.ApelMat);
                            $("#prospecto").val(data.data.Id_Guest);
                            $("#direccion").val(data.data.Direccion);
                            
                            $("#departamento").val(data.data.direccion_departamento);
                            $("#distrito").val(data.data.direccion_distrito);
                            $("#provincia").val(data.data.direccion_provincia);
                            $("#textUbigeo").val(data.data.direccion_departamento+" - "+data.data.direccion_provincia+" - "+data.data.direccion_provincia);
                            $("#email").val(data.data.Email);
                            $("#estado_civil").val(data.data.C_EstadoCivil);
                            $("#fecha_nacimiento").val(data.data.Fecha_Nacimiento);
                            $("#hijos").val(data.data.Hijos);
                            $("#idpersona").val(data.data.Id_Guest);

                            $("#nacionalidad").val(data.data.nacionalidad);
                            
                            $("#sexo").val(data.data.Sexo);
                            $("#nombre").val(data.data.Nombres);
                            $("#num_documento").val(data.data.DNI);
                            $("#parentesco").val(data.data.parentesco);
                            $("#ocupacion").val(data.data.Ocupacion);
                            $("#telefono").val(data.data.Telefono02);
                            $("#tipo_documento").val(data.data.TipoDocumento);
                        }else{
                            swal('No existe DNI registrado');
                        }
                    },
                    error   : function(jqxhr, textStatus, error){
                        console.log(jqxhr.responseText);
                    }
                });
            }

        });

        $(document).on('submit','#frm_nota',function(e){
            e.preventDefault();
            let urlnota = $(this).attr('action');
            let data = $(this).serialize();
            console.log(urlnota);
            let perso = $('#persoid').val();
            if(perso == ''){
                swal("Debe de seleccionar un codigo de procedencia");
            }else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url     : urlnota,
                    type    : 'POST',
                    dataType: 'JSON',
                    data    : data+'&persoid='+perso,
                    beforeSend : function(data){
                        $(".loading").show();
                    },
                    success : function(data){
                        
                        console.log(data);
                        let vhtml = '';
                        $( data.data ).each(function( index, element ){
                            vhtml += '<tr>';
                            vhtml += '<td>'+element.asunto+'</td>'
                            vhtml += '<td>'+element.asunto+'</td>'
                            vhtml += '<td>'+element.informacion+'</td>'
                            vhtml += '<td>'+element.fecha_seguimiento+'</td>'
                            vhtml += '<td>'+element.fecha_registro+'</td>'
                            vhtml += '<td>'+element.usuario_creacion+'</td>'
                            vhtml += '</tr>';
                        });
                        $('#lstNota').html(vhtml);
                        $('#modalNota').modal('hide');
                        $('#asunto').val("");
                        $('#mensaje').val("");
                        $('#calificacion').val("");
                        $(".loading").hide();
                    },
                    error   : function(jqxhr, textStatus, error){
                        console.log(jqxhr.responseText);
                    }
                });
            }

            return false;
        });

        $('#tranferir-data').click(function(e){
            e.preventDefault();
            $(".loading").show();
            $('.loading h3').text('TRANSFIRIENDO DATOS...');

            let codigo = $('#persoid').val();
            if(codigo != ''){
                // alert(codigo);
                location.href="transferencia/"+codigo;
            }else{
                swal("Debe de seleccionar un usuario");
            }
        });

    });
</script>