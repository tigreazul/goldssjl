<div class="modal fade" id="modalProcedencia" aria-labelledby="myModalLabel" aria-hidden="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modal-title">CONSULTA DE PROSPECTOS</h4>
            </div>
            <div class="modal-body">
                <!-- <table id="lstprospecto" class="table hover table-bordered table-striped"> -->
                <table id="lstprospecto" class="table hover table-bordered table-striped display" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Seleccione</th>
                        <th>Nombres y Apellidos</th>
                        <th>Documento</th>
                        <th>Codigo Prospecto</th>
                    </tr>
                    </thead>
                    <tbody class="data-prospecto">
                        <!-- @foreach($persona as $perso)
                            <tr>
                              <td><a href="#" class="btn btn-warning seleccion" data-id="{{ $perso->Id_Guest}}" class="selProspecto"><i class="fa fa-check"></i></a></td>
                              <td>{{ $perso->NombreCompleto }}</td>
                              <td>{{ $perso->DNI }}</td>
                              <td>{{ $perso->Id_Guest }}</td>
                            </tr>
                        @endforeach -->
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>Seleccione</th>
                        <th>Nombres y Apellidos</th>
                        <th>Documento</th>
                        <th>Codigo Prospecto</th>
                    </tr>
                    </tfoot>
              </table>            
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="modalUbigeo" aria-labelledby="myModalLabel" aria-hidden="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modal-title">UBIGEO</h4>
            </div>
            <div class="modal-body">
                <div class="form">
                    <div class="modal-body">
                        <div class="form-group has-success">
                            <label for="recipient-name" class="control-label">Departamento:</label>
                            <select name="departamentox" id="departamentos" class="form-control" required="required">
                                <option value="">[SELECCIONE]</option>
                                @foreach($departamento as $dep)
                                    <option value="{{ $dep->descripcion }}" data-id="{{ $dep->id }}">{{ $dep->descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group has-success">
                            <label for="recipient-name" class="control-label">Provincia:</label>
                            <select name="provinciax" id="provincias" class="form-control" required="required" disabled>
                                <option value="">[SELECCIONE]</option>
                            </select>
                        </div>
                        <div class="form-group has-success">
                            <label for="recipient-name" class="control-label">Distrito:</label>
                            <select name="distritox" id="distritos" class="form-control" required="required" disabled>
                                <option value="">[SELECCIONE]</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer" id="modal-footer">
                        <input type="hidden" name="id">
                        <a class="btn btn-primary" id="addUbigeo"><i class="fa fa-save"></i> Registrar</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="modalNota" aria-labelledby="myModalLabel" aria-hidden="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modal-title">NOTAS</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('prospecto.nota') }}" class="form" method="POST" id="frm_nota" accept-charset="UTF-8">
                    <div class="modal-body">
                        <div class="form-group has-success">
                            <div class="col-md-2">
                                <label for="recipient-name" class="control-label">Asunto:</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" name="asunto" id="asunto" class="form-control" required="required">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group has-success">
                            <div class="col-md-2">
                                <label for="recipient-name" class="control-label">Mensaje:</label>
                            </div>
                            <div class="col-md-10">
                                <textarea class="form-control" name="mensaje" id="mensaje" rows="8" required="required"></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group has-success">
                            <div class="col-md-2">
                                <label for="recipient-name" class="control-label">Calificación:</label>
                            </div>
                            <div class="col-md-10">
                                <select class="form-control" name="calificacion" id="calificacion" required="required">
                                    <option value="">[SELECCIONE]</option>
                                    @foreach($calificacion as $cal)
                                        <option>{{ $cal->TITULO }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="modal-footer" id="modal-footer">
                        <input type="hidden" name="idnota" id="idnota">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Grabar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<style>
    .vhead {
        background: #505759;
        color: #fff;
        text-align: left;
        font-weight: bold;
        border: 1px solid #505759;
        text-transform: uppercase;
    }
    .content-table{
        border: 1px solid #505759;
    }
    .fc-content{
        padding: 5px !important;
    }
</style>