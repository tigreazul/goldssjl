@extends('layout.default')
@section('content')

    <section class="content-header" style="margin-bottom: 20px;">
        <h1>
            Transferencia
            <small>registro</small>
        </h1>
    </section>

    <div class="col-sm-12">
        @if (Session::has('success'))
            @php
                $class 		= session('success') ? 'alert-success': (session('error') ? 'alert-danger': (session('warning') ? 'alert-warning': ''));
                $class_icon = session('success') ? 'check': (session('error') ? 'exclamation-circle': (session('warning') ? 'exclamation-triangle': ''));
                $message 	= session('success') ? session('success'): (session('error') ? session('error'): (session('warning') ? session('warning'): 'No hay mensaje'));
            @endphp
            <div class="alert {!! $class !!} text-center">  
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> 
                <p><span class="fa fa-{!! $class_icon !!}"></span> {!! $message !!}</p>   
            </div>
        @endif
        @if ($errors->any())
            <div class="alert text-center">
                <h6>Por favor corrige los errores debajo:</h6>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div> 
        @endif
    </div>

    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-green">
                        <img src="http://placehold.it/258x258" style="width:100%" class="img-rounded">
                        <div class="widget-user-image">
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username cards">Codigo: <strong>5258</strong></h3>
                        <h3 class="widget-user-username cards">{{ $persona->nombre." ".$persona->apellido_paterno." ".$persona->apellido_materno}}</h3>
                        <h5 class="text-center">Lead Developer</h5>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="#">Fecha de registro <span class="pull-right badge bg-blue">10-10-2019</span></a></li>
                            <li><a href="#">Ultimo ingreso <span class="pull-right badge bg-red">25-10-2018</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Información del cliente</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Nota</a></li>
                        <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Membresias(NZ)</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box box-default">
                                <div class="box-body no-padding">
                                    <form action="{{ route('prospecto.registro') }}" class="form" method="POST" id="formProspecto" accept-charset="UTF-8" >
                                        {{ csrf_field() }}
                                        <div class="modal-body">

                                            <h4 class="page-header">Datos Personales</h4>

                                            <div class="form-group has-success">
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Documento:</label>
                                                    <select class="form-control" name="documento" id="tipo_documento" required disabled>
                                                        
                                                        @foreach($tdocument as $tdoc)
                                                            <option value="{{ $tdoc->TITULO }}">{{ $tdoc->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <div class="">
                                                        <label for="recipient-name" class="nopadding control-label">Nº Documento:</label>
                                                        <input type="text" name="nro_documento" maxlength="11" class="form-control" value="{{ $persona->num_documento }}" required="required" disabled>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <div class="">
                                                        <label for="recipient-name" class="control-label">Distrito:</label>
                                                        <input type="text" name="textUbigeo" id="textUbigeo" class="form-control" required="required" value="{{ $persona->direccion_departamento.' - '.$persona->direccion_provincia.' - '.$persona->direccion_distrito }}" disabled>
                                                        <input type="hidden" name="distrito" id="distrito" class="form-control" required="required">
                                                        <input type="hidden" name="provincia" id="provincia" class="form-control" required="required">
                                                        <input type="hidden" name="departamento" id="departamento" class="form-control" required="required">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Nombre:</label>
                                                    <input type="text" name="nombre" id="nombre" class="form-control" required="required" value="{{ $persona->nombre }}" disabled>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Apellido Paterno:</label>
                                                    <input type="text" name="ap_paterno" id="apellido_paterno" class="form-control" required="required" value="{{ $persona->apellido_paterno }}" disabled>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Apellido Materno:</label>
                                                    <input type="text" name="ap_materno" id="apellido_materno" class="form-control" required="required" value="{{ $persona->apellido_materno }}" disabled>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">
                                                <div class="col-md-12 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Dirección:</label>
                                                    <input type="text" name="direccion" id="direccion" class="form-control" required="required" value="{{ $persona->direccion_calle }}" disabled>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Fecha Nacimiento:</label>
                                                    <input type="date" name="fnacimiento" id="fecha_nacimiento" class="form-control" required="required" value="{{ $persona->fecha_nacimiento }}" disabled>
                                                </div>
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Sexo:</label>
                                                    <select class="form-control" name="sexo" id="sexo" required disabled>
                                                        
                                                        @foreach($sexo as $sex)
                                                            <option value="{{ $sex->TITULO }}">{{ $sex->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Estado Civil:</label>
                                                    <select class="form-control" name="ecivil" id="estado_civil" required disabled>
                                                        
                                                        @foreach($ecivil as $civil)
                                                            <option value="{{ $civil->TITULO }}">{{ $civil->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Telefono:</label>
                                                    <input type="text" name="telefono" id="telefono" class="form-control" required="required" value="{{ $persona->telefono }}" disabled>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Email:</label>
                                                    <input type="text" name="email" id="email" class="form-control" required="required" value="{{ $persona->email }}" disabled>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Parentesco:</label>
                                                    <select class="form-control" name="parentesco" id="parentesco" required disabled>
                                                        
                                                        @foreach($parentesco as $parent)
                                                            <option value="{{ $parent->TITULO }}">{{ $parent->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>


                                            <div class="form-group has-success">

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Ocupación:</label>
                                                    <select class="form-control" name="ocupacion" id="ocupacion" required disabled>
                                                        
                                                        @foreach($ocupacion as $ocupa)
                                                            <option value="{{ $ocupa->TITULO }}">{{ $ocupa->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Hijos:</label>
                                                    <select class="form-control" name="hijos" id="hijos" required disabled>
                                                        
                                                        @foreach($hijos as $hijo)
                                                            <option value="{{ $hijo->TITULO }}">{{ $hijo->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                        <label for="recipient-name" class="control-label">Nacionalidad:</label>
                                                    <select class="form-control" name="nacionalidad" id="nacionalidad" required disabled>
                                                        
                                                        @foreach($nacionalidad as $nac)
                                                            <option value="{{ $nac->TITULO }}">{{ $nac->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <hr> 
                                            <div class="form-group has-success">
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Procedencia:</label>
                                                    <select class="form-control" name="procedencia" id="procedencia" required disabled>
                                                        @foreach($procedencia as $proc)
                                                            <option value="{{ $proc->codigo }}">{{ $proc->procedencia }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Sub Procedencia:</label>
                                                    <select class="form-control" name="subprocedencia" id="subprocedencia" required disabled>
                                                        
                                                        @foreach($subprocedencia as $sub)
                                                            <option value="{{ $sub->TITULO }}">{{ $sub->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                
                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="nopadding control-label">Corporativo:</label>
                                                    <select class="form-control" name="corporativo" id="corporativo" required disabled>
                                                        
                                                        @foreach($corporativo as $corp)
                                                            <option value="{{ $corp->TITULO }}">{{ $corp->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group has-success">

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Campaña:</label>
                                                    <select class="form-control" name="campania" id="campania" required disabled>
                                                        
                                                        @foreach($campania as $camp)
                                                            <option value="{{ $camp->TITULO }}">{{ $camp->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Nº Hoja de Atención:</label>
                                                    <input type="text" name="nro_hoja" id="nro_hoja_atencion" class="form-control times" required="required" value="{{ $persona->nro_hoja_atencion }}" disabled>
                                                </div>

                                                <div class="col-md-4 nopadding">
                                                    <label for="recipient-name" class="control-label">Fitness Consultant:</label>
                                                    <select class="form-control" name="fitness" id="fitness" required disabled>
                                                        
                                                        @foreach($fitness as $fit)
                                                            <option value="{{ $fit->TITULO }}">{{ $fit->TITULO }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                            </div>

                                        </div>

                                        <div class="modal-footer" id="modal-footer">
                                            <input type="hidden" name="id">
                                            <!-- <button type="submit" class="btn btn-success" id="nuevo" style="display: none;"><i class="fa fa-newspaper-o"></i> Nuevo</button> -->
                                            <!-- <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Grabar</button> -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane " id="tab_2">
                            
                            <!-- <a href="#" class="btn btn-primary" style="margin-bottom: 10px" data-toggle="modal" data-target="#modalNota"><i class="fa fa-book"></i> NUEVA NOTA</a> -->

                            <div class="box box-default">
                                <div class="box-body no-padding">
                                    <table id="example2" class="table table-bordered table-striped" style="padding-top: 20px;">
                                        <thead>
                                            <tr class="bg-black disabled color-palette">
                                                <th>Tipo</th>
                                                <th>Asunto</th>
                                                <th>Información</th>
                                                <th>Fecha Seguimiento</th>
                                                <th>Fecha Registro</th>
                                                <th>Usuario Creación</th>
                                            </tr>
                                        </thead>
                                        <tbody id="lstNota">
                                            @foreach($notas as $nota)
                                            <tr>
                                                <td>{{ $nota->asunto }}</td>
                                                <td>{{ $nota->asunto }}</td>
                                                <td>{{ $nota->informacion }}</td>
                                                <td>{{ $nota->fecha_seguimiento }}</td>
                                                <td>{{ $nota->fecha_registro }}</td>
                                                <td>{{ $nota->usuario_creacion }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <!-- /.tab-pane -->
                        <!-- tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="box box-default">
                                <div class="box-body no-padding">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr class="bg-black disabled color-palette">
                                                <th style="width: 10px">#</th>
                                                <th>Nombre membresia</th>
                                                <th>Fecha Inicio</th>
                                                <th>Fecha Fin</th>
                                                <th>Monto</th>
                                            </tr>
                                            @foreach($membresia as $memb)
                                                <option value="{{ $memb->idarticulo }}"></option>
                                                <tr class="bg-red disabled color-palette">
                                                    <td>1.</td>
                                                    <td>{{ $memb->descripcion }}</td>
                                                    <td>{{ $memb->fecha_inicio }}</td>
                                                    <td>{{ $memb->fecha_fin }}</td>
                                                    <td>{{ $memb->monto }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    @include('transferido.script')
    

    <style type="text/css">
        .cards{
            margin: 15px 0px 15px 0px !important;
            text-align: center;
        }
    </style>
    
@stop