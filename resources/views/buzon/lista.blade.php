@extends('layout.default')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $buzon->titulo_convocatoria}}
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <a href="{{ route('compra.enviados')}}" class="btn btn-default">Volver</a>
                        <hr>
                        {{-- {{ $sql }} --}}
                        <table id="tableCompras" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>RUC</th>
                                    <th>Razon Social</th>
                                    <th>Declaración Jurada</th>
                                    <th>CCI</th>
                                    <th>Cotización</th>
                                    <th>Carta Cotización</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($provider as $prov)
                                    <tr>
                                        <td>{{ $prov->RUC }}</td>
                                        <td>{{ $prov->Razon_social }}</td>
                                        <td>
                                            @if ( $prov->DJ != '' )
                                                <a href="{{ url("../storage/app/".$prov->DJ) }}" target="_blank" class="btn btn-info"><i class="fa fa-print"></i> Imprimir</a>
                                            @else
                                                <p class="error"><i class="fa fa-remove"></i> NO ENVIO DOCUMENTO</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if ( $prov->CCI != '' )
                                                <a href="{{ url("../storage/app/".$prov->CCI) }}" target="_blank" class="btn btn-info"><i class="fa fa-print"></i> Imprimir</a>
                                            @else
                                                <p class="error"><i class="fa fa-remove"></i> NO ENVIO DOCUMENTO</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if ( $prov->Cotiza != '' )
                                                <a href="{{ url("../storage/app/".$prov->Cotiza) }}" target="_blank" class="btn btn-info"><i class="fa fa-print"></i> Imprimir</a>
                                            @else
                                                <p class="error"><i class="fa fa-remove"></i> NO ENVIO DOCUMENTO</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if ( $prov->Carta != '' )
                                                <a href="{{ url("../storage/app/".$prov->Carta) }}" target="_blank" class="btn btn-info"><i class="fa fa-print"></i> Imprimir</a>
                                            @else
                                                <p class="error"><i class="fa fa-remove"></i> NO ENVIO DOCUMENTO</p>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    <!-- /.row -->
    </section>
    <!-- /.content -->
    <style>
        #tableCompras tr th{
            background: silver;
            text-transform: uppercase;
        }
        .error{
            color: red;
            font-size: 12px;
        }
    </style>
    <script>
        $('body').on('click','.enviom',function(){
            let code = $(this).data('id');
            console.log(code);
            if(confirm('Al hacer el envio del correo se cerrara la edición de la convocatoria') == true){
                $.ajax({
                    url: local.base+'/api/enviarEmail',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        id: code
                    },
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success: function(data){
                        // console.log();
                        if(data.status == true){
                            $('.loading').hide();
                            location.href = local.base;
                        }
                    }
                });
            }
        });
    </script>
@stop