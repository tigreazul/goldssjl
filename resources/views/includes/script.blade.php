<!-- ./wrapper -->
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>


<!-- bootstrap datepicker -->
<script src="{{ asset('vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- bootstrap time picker -->
<script src="{{ asset('vendor/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- bootstrap datetimepicker -->
<script src="{{ asset('vendor/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ asset('vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('vendor/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('js/demo.js') }}"></script>


{{-- Calendario --}}
<script src="{{ asset('plugins/jQuery/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>

<script src="{{ asset('plugins/jQuery/moment.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/lang-all.js') }}"></script>

{{-- <script src="{{ asset('vendor/tag-input/tagInput.js') }}"></script> --}}

<!-- page script -->
<script>
    $(function () {

        $('#tableCompra').DataTable({
            "order": [[ 0, "desc" ]]
        });
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
        //Timepicker
        $('.timepicker').timepicker({
            // showInputs: false
        });

        $('#datetimepicker3').datetimepicker({
            pickDate: false
        });

    });

    $('#lstContrato').DataTable({
        "lengthMenu": [[20, 50, 100, 150], [20, 50, 100, 150]],
        'paging'      : true,
        // 'lengthChange': false,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        // 'autoWidth'   : false
    });

    var modal_helper = function(modal_id_or_class){
        // Objeto modal
        var $modal_object 	= $(modal_id_or_class);
        // Opciones
        var options 		= {
            modal 		: $modal_object,
            header		: $modal_object.find('.modal-header'),
            message		: $modal_object.find('.modal-message'),
            body		: $modal_object.find('.modal-body'),
            footer		: $modal_object.find('.modal-footer'),
            form		: $modal_object.find('form'),
            action 		: 'agregar',
            input_text 	: '',
            render_form	: function() {
                var $this = this;

                // Crea elementos de tipo input
                $.each(this.modal.find('input'), function(e, val){ 
                    $this[ this.name ] = $(this);	
                });

                // Crea elementos de tipo select
                $.each(this.modal.find('select'), function(e, val){ 
                    $this[ this.name ] = $(this);
                });

                // Crea elementos de tipo select  
                $.each(this.modal.find('textarea'), function(e, val){  
                    $this[ this.name ] = $(this); 
                });
                // Crea elementos tipo tabla
                $.each(this.modal.find('table'), function(e, val){  
                    $this[ this.id ] = $(this); 
                });
            },
            render_html : function(render_obj) {
    
                if (!render_obj instanceof Object) return; 
                if (!'name' in render_obj) return;
                if (!'data' in render_obj) return; 

                var $object = /^(\.|\#)/.test(render_obj.name) ? $(render_obj.name): this[ render_obj.name ] ; 	

                if ('action' in render_obj && render_obj.action == 'append') {
                    $object.append(htmlspecialchars_decode(render_obj.data)); 
                } else {
                    $object.html(htmlspecialchars_decode(render_obj.data)); 	
                }
            },
            input 		: function(obj) {
                
                if ( ! obj instanceof Object) return;
                if ( ! 'name' in obj ) return;
                if ( ! 'value' in obj ) return;

                console.log(obj);

                this.modal.find('input[name="'+obj.name+'"]').val(obj.value);
            },
            set_item	: function(item_name, item_id_or_class) {
                this[ item_name ] = this.modal.find(item_id_or_class)
            },
            show_title	: function(title_text) {
                this.header.find('.header-title').text(title_text).fadeIn();
            },
            hide_title	: function() {
                this.header.find('.header-title').hide();
            },
            show_message: function(message_object) {

                if (!message_object instanceof Object) return;
                if (!'text' in message_object) return;

                if ('status' in message_object) {
                    // Success message
                    if (message_object.status == 'success') {
                        this.message.find('.alert').removeClass('alert-danger');
                        this.message.find('.alert').addClass('alert-success');
                    }
                    // Error message
                    if (message_object.status == 'error') {
                        this.message.find('.alert').removeClass('alert-success');
                        this.message.find('.alert').addClass('alert-danger');
                    }
                } 

                this.message.find('.text-message').text(message_object.text);
                this.message.fadeIn();
            },
            hide_message: function() {
                this.message.hide();
            },
            set_action 	: function(action_value){
                this.modal.find('input[name="action"]').val(action_value);
            }, 
            set_id 	: function(action_value){
                this.modal.find('input[name="id"]').val(action_value);
            }, 
            get_action 	: function()
            {
                return this.modal.find('input[name="action"]').val();
            },
            set_input 	: function(input_object) {

                if (!input_object instanceof Object) return;
                if (!'content' in input_object) return;
                if (!'name' in input_object) return;
                if (!'value' in input_object) return;

                var $object = /^(\.|\#)/.test(input_object.content) ? $(input_object.content): this[ input_object.content ] ; 

                this.input_text = input_object.name;

                $object.prepend('<input type="hidden" name="'+input_object.name+'" id="'+input_object.name+'" value="'+input_object.value+'" class="fake-item" />')
            },
            get_input 	: function()
            {
                return this.modal.find('input[name="'+this.input_text+'"]').val();
            },
            btn_reset	: function(btn_object){
                if (!btn_object instanceof Object) return;
                if (!'action' in btn_object) return;

                if (btn_object.action == 'enable') {
                    this.footer.find('button[type="reset"]').removeAttr('disabled');
                }

                if (btn_object.action == 'disable') {
                    this.footer.find('button[type="reset"]').attr('disabled', true);
                }	
            },
            disableSubmitButton : function(){
                this.modal.find('button[type="submit"]').attr('disabled', true);
            },
            enableSubmitButton : function(){
                this.modal.find('button[type="submit"]').attr('disabled', false);
            },
            restore 	: function() {
                
                if (this.form.length > 0)
                {
                    // Reset
                    this.form[0].reset(); 
                }
                
                // Hide
                this.hide_message(); 
                this.hide_title();
                //
                this.set_action(this.action);
                //
                this.btn_reset({action: 'enable'});
                this.enableSubmitButton();
                //
                this.modal.find('.fake-item').remove();
            },
            onHide 	: function(e){
                this.modal.on('hide.bs.modal', e);
            }
        };
    
        options.render_form(); 

        return options;
    };
    
</script>