<!-- Logo -->
<a href="" class="logo">
<!-- mini logo for sidebar mini 50x50 pixels -->
<span class="logo-mini"><b>GYM</b></span>
<!-- logo for regular state and mobile devices -->
<span class="logo-lg"><b>GYM</b></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        
        <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
                    <span class="hidden-xs">{{ $username }}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="{{ asset('images/logo.png') }}" class="img-circle" alt="User Image">
                        <p>
                            {{ $username }}<small> {{ $usuario->tipo_usuario }}, GLOBAL FRANQUICIAS INTERNACIONAL SAC {{ $usuario->direccion }}</small>
                        </p>
                    </li>
                  
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="http://localhost:8888/gym/acceder.php" class="btn btn-default btn-flat">Cambiar Sucursal</a>
                        </div>
                        <div class="pull-right">
                            <a href="http://localhost:8888/gym/ajax/UsuarioAjax.php?op=Salir" class="btn btn-default btn-flat">Salir</a>
                        </div>
                    </li>
                </ul>
                <!-- <ul class="dropdown-menu" style="width: 180px;">
                    <li class="user-body">
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <a href="{{route('login.logout')}}">Cerrar Sesion</a>
                            </div>
                        </div>
                    </li>
                </ul> -->
            </li>
        </ul>
    </div>
</nav>