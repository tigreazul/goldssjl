<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <head>
        <meta name="viewport" content="width=device-width"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>SBN</title>


        <style type="text/css">
            img {
                max-width: 100%;
            }

            body {
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: none;
                width: 100% !important;
                height: 100%;
                line-height: 1.6em;
            }

            body {
                background-color: #f6f6f6;
            }

            @media only screen and (max-width: 640px) {
                body {
                    padding: 0 !important;
                }

                h1 {
                    font-weight: 800 !important;
                    margin: 20px 0 5px !important;
                }

                h2 {
                    font-weight: 800 !important;
                    margin: 20px 0 5px !important;
                }

                h3 {
                    font-weight: 800 !important;
                    margin: 20px 0 5px !important;
                }

                h4 {
                    font-weight: 800 !important;
                    margin: 20px 0 5px !important;
                }

                h1 {
                    font-size: 22px !important;
                }

                h2 {
                    font-size: 18px !important;
                }

                h3 {
                    font-size: 16px !important;
                }

                .container {
                    padding: 0 !important;
                    width: 100% !important;
                }

                .content {
                    padding: 0 !important;
                }

                .content-wrap {
                    padding: 10px !important;
                }

                .invoice {
                    width: 100% !important;
                }
            }
        </style>
    </head>

<body style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f7f7f7; margin: 0;" bgcolor="#f7f7f7">

<table class="body-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f7f7f7">
    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
        <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
        <td class="container" width="900" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 900px !important; clear: both !important; margin: 0 auto;" valign="top">
            <div class="content" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 900px; display: block; margin: 0 auto; padding: 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope itemtype="http://schema.org/ConfirmAction" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; margin: 0; border: none;">
                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;padding: 30px;border: 2px solid #ddd;border-radius: 4px; background-color: #fff;" valign="top">
                            <meta itemprop="name" content="Confirm Email" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"/>
                            {{-- {{ dd($provider) }} --}}
                            @php
                                setlocale(LC_TIME, 'spanish')
                            @endphp
                            <p>San Isidro, {{ date('d')}} de {{ ucwords(strftime("%B",mktime(0, 0, 0, date('m'), 1, 2000))) }} del {{ date('Y') }}</p>
                                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                    <tr>
                                        <td style="text-align: center">
                                            <!-- <a href="#" style="display: block;margin-bottom: 10px;"> <img src="https://academiavirtual10.com/imgs/logo.png" alt="logo"/></a> <br/> -->
                                        </td>
                                    </tr>
                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                            <p>Señor(es): <strong>{{ strtoupper($provider->Razon_social) }}</strong></p>
                                            <p>El presente es para solicitar nos remitan una cotización/propuesta técnica-económica por la contratación de <strong>{{ strtoupper($provider->titulo_convocatoria) }}</strong> para la Superintendencia Nacional de Bienes Estatales.</p>
                                        </td>
                                    </tr>
                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; text-align: justify;" valign="top">
                                            <p>Al respecto, SOLO deberá remitir su cotización a la entidad a través del correo electrónico: adq8uit@sbn.gob.pe, a más tardar hasta el día <strong> {{ \Carbon\Carbon::parse($provider->fecha_maxima)->format('d/m/Y') }}</strong> a horas <strong>12.00PM</strong>, o a través de la Unidad de Tramite Documentario sito en calle 
                                            Chinchón N° 890 San Isidro, dirigido a la Oficina de Administración y Finanzas, indicando en ambos casos en el asunto 
                                            la palabra COTIZACION seguida del (objeto de la contratación), adjuntando el ANEXO N° 9 y todo documento necesario que permita establecer de manera indubitable las características 
                                            del bien o el servicio ofertado, pudiendo utilizar para ello cualquier formato.</p>
                                        </td>
                                    </tr>
                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; text-align: justify;" valign="top">
                                            <p>Se remite en archivo adjunto los términos de referencia/ especificaciones técnicas y otros documentos anexos.</p>
                                        </td>
                                    </tr>
                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; text-align: justify;" valign="top">
                                            <p>En caso la cotización sea remitida por correo electrónico, además del correo establecido, deberá comunicarlo a la dirección electrónica o correo electrónico que lo invita a cotizar, sin adjuntar el documento alguno, caso contrario no será considerado como propuesta valida.</p>
                                        </td>
                                    </tr>
                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; text-align: justify;" valign="top">
                                            <p>Quedamos a la espera de su pronta respuesta.</p>
                                        </td>
                                    </tr>
                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                            <p>Ruta de acceso: <strong><a href="{{ route('announcement.listado') }}" target="_blank" style="color: black;">http://sbn.gob.pe/convocatoria</a></strong></p>
                                            <p>Su clave de acceso es: <strong>{{ $provider->Clave }}</strong></p>
                                        </td>
                                    </tr>

                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                            <h4>FIRMA </h4>
                                        </td>
                                    </tr>
                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                        @if( $provider->usuario_creacion == 'emalaga')
                                            <img src="{{ url('firmas/sofia_malaga.jpg')}}" alt="{{$provider->usuario_creacion}}" style="width: 250px;">
                                        @endif

                                        @if( $provider->usuario_creacion == 'gsoto')
                                            <img src="{{ url('firmas/chris_soto.jpg')}}" alt="{{$provider->usuario_creacion}}" style="width: 250px;">
                                        @endif
                                        </td>
                                    </tr>
                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                            <h4>NOTA:</h4>
                                            <ul>
                                                <li>La contratación deberá indicar el costo de la contratación y deberá estar firmado, según corresponda</li>
                                                <li>El precio deberá incluir IGV, todos los tributos, pruebas, trasporte, inspecciones, así como cualquier otro concepto que se le sea aplicable y que pueda 
                                                incidir sobre el valor del bien o servicio, según corresponda; por lo que, se considerará que el precio de cotización remitido incluye los conceptos antes establecidos.</li>
                                                <li>El PROVEEDOR deberá precisar que cumple con integridad los requerimientos técnicos mínimos (Términos de referencia o especificaciones técnicas</li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                        </td>
                    </tr>
                </table>
                <div class="footer" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
                </div>
            </div>
        </td>
        <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
    </tr>
</table>
</body>
</html>
