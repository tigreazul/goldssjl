/*
Navicat MySQL Data Transfer

Source Server         : localwamp
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : calendar

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-11-15 12:44:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_calendar`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_calendar`;
CREATE TABLE `tbl_calendar` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CARGO` int(11) DEFAULT NULL,
  `TITULO` varchar(150) DEFAULT NULL,
  `DESCRIPCION` text,
  `FECHA_INICIO` datetime DEFAULT NULL,
  `FECHA_FIN` datetime DEFAULT NULL,
  `FECHA_REGISTRO` datetime DEFAULT NULL,
  `ESTADO` int(11) DEFAULT NULL,
  `HORA_INICIO` varchar(50) DEFAULT NULL,
  `HORA_FIN` varchar(50) DEFAULT NULL,
  `FECHA_MODIFICACION` datetime DEFAULT NULL,
  `COLOR` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_calendar
-- ----------------------------
INSERT INTO `tbl_calendar` VALUES ('1', '1', 'Reunion en el ministero', 'REUNION', '2018-11-11 12:34:47', '2018-11-13 12:34:54', '2018-11-12 12:35:03', '1', '11:11', '23:11', null, '#f56954');
INSERT INTO `tbl_calendar` VALUES ('2', '2', 'p', 'iii', '2018-11-15 00:00:00', '2018-11-16 00:00:00', '2018-11-12 00:00:00', '0', '16:55', '16:55', '2018-11-12 22:56:51', '#f56954');
INSERT INTO `tbl_calendar` VALUES ('3', '1', 'p', 'iii', '2018-11-15 00:00:00', '2018-11-16 00:00:00', '2018-11-12 00:00:00', '0', '16:55', '16:55', '2018-11-12 23:03:38', '#f39c12');
INSERT INTO `tbl_calendar` VALUES ('4', '1', 'p', 'iii', '2018-11-15 00:00:00', '2018-11-16 00:00:00', '2018-11-12 00:00:00', '0', '16:55', '16:55', '2018-11-12 22:56:42', '#f56954');
INSERT INTO `tbl_calendar` VALUES ('5', '1', 'lll', 'lñlñ', '2018-11-15 00:00:00', '2018-11-17 00:00:00', '2018-11-12 00:00:00', '0', '18:00', '18:00', '2018-11-12 23:03:32', '#f39c12');
INSERT INTO `tbl_calendar` VALUES ('6', '2', 'rrrr', 'cccc', '2018-11-21 00:00:00', '2018-11-24 00:00:00', '2018-11-12 00:00:00', '0', '18:04', '18:04', '2018-11-13 13:35:37', '#f39c12');
INSERT INTO `tbl_calendar` VALUES ('7', '2', 'hhhhddd', 'ffff', '2018-11-15 00:00:00', '2018-11-16 00:00:00', '2018-11-12 00:00:00', '0', '18:04', '18:04', '2018-11-13 13:35:32', '#f56954');
INSERT INTO `tbl_calendar` VALUES ('8', '2', 'HOLA3333', 'FFFFFcvgvergr', '2018-11-15 00:00:00', '2018-11-16 00:00:00', '2018-11-12 00:00:00', '1', '06:04', '12:04', null, '#f39c12');
INSERT INTO `tbl_calendar` VALUES ('9', '1', 'vvv', 'eddd', '2018-11-06 00:00:00', '2018-11-07 00:00:00', '2018-11-13 00:00:00', '0', '01:00', '12:59', '2018-11-13 16:53:08', '#f39c12');
INSERT INTO `tbl_calendar` VALUES ('10', '1', 'xccc', 'cvv', '2018-11-21 00:00:00', '2018-11-22 00:00:00', '2018-11-13 00:00:00', '1', '12:45', '12:45', null, '#f56954');
INSERT INTO `tbl_calendar` VALUES ('11', '2', 'COMUNICADO', 'fffff', '2018-11-07 00:00:00', '2018-11-09 00:00:00', '2018-11-13 00:00:00', '1', '16:20', '16:20', null, '#f39c12');

-- ----------------------------
-- Table structure for `tbl_usuario`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_usuario`;
CREATE TABLE `tbl_usuario` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERID` varchar(20) DEFAULT NULL,
  `CORREO` varchar(100) DEFAULT NULL,
  `AUTORIZACION` int(11) DEFAULT NULL,
  `MODULO` varchar(20) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `FECHA_CREACION` datetime DEFAULT NULL,
  `ESTADO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_usuario
-- ----------------------------
INSERT INTO `tbl_usuario` VALUES ('1', 'jtellot', 'jtellot@sbn.gob.pe', '1', '1', 'Junior Tello Trujillo', '2018-11-12 11:40:19', '1');
