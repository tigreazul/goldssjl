<?php

Class Funciones{
    //Funciones::saluda()
    public static function saluda(){
        return 'Hola';
    }

    public static function fecha_sql($fecha){
        date_default_timezone_set('America/Lima');
        $fecha = explode('/',$fecha);
        $fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
        $datetime_variable = new DateTime($fecha);
        $datetime_formatted = $datetime_variable->format('Y-m-d H:i:s');
        return  $datetime_formatted;  
    }

    public static function addDay($date,$days){
        date_default_timezone_set('America/Lima');
        $nuevafecha = strtotime ( '+'.$days.' day' , strtotime ( $date ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        return  $nuevafecha; 
    }

    public static function sesiones(){
        if (\Session::has('userId')){
            return 'true';
        }else{
            return 'false';
        }
    }
}