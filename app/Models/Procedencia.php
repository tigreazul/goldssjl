<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Procedencia extends Model
{
    protected $table = 'procedencia',
    $primaryKey = 'idProcedencia';
    public $timestamps = false;
}
