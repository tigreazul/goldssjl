<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $table = 'articulo',
    $primaryKey = 'idarticulo';
    public $timestamps = false;
}
