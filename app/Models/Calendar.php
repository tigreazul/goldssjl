<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $table = 'TBL_CALENDAR',
    $primaryKey = 'ID';
    public $timestamps = false;
}
