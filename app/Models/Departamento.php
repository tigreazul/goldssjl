<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'tbl_departamento',
    $primaryKey = 'pk_dep_int_id';
    public $timestamps = false;
}
