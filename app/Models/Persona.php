<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'persona',
    $primaryKey = 'idpersona';
    public $timestamps = false;
}
