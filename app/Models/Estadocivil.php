<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estadocivil extends Model
{
    protected $table = 'estado_civil',
    $primaryKey = 'idEstadoCivil';
    public $timestamps = false;
}
