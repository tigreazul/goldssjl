<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subprocedencia extends Model
{
    protected $table = 'subprocedencia',
    $primaryKey = 'IdSubProcedencia';
    public $timestamps = false;
}
