<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campania extends Model
{
    protected $table = 'campania',
    $primaryKey = 'idCampana';
    public $timestamps = false;
}
