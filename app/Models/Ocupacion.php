<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ocupacion extends Model
{
    protected $table = 'ocupacion',
    $primaryKey = 'idOcupacion';
    public $timestamps = false;
}
