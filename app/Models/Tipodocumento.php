<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipodocumento extends Model
{
    protected $table = 'tipo_documento',
    $primaryKey = 'idtipo_documento';
    public $timestamps = false;
}
