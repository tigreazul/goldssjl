<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $table = 'tbl_provincia',
    $primaryKey = 'pk_prv_int_id';
    public $timestamps = false;
}
