<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prospecto extends Model
{
    // protected $table = 'prospecto',
    // $primaryKey = 'idprospecto';
    protected $table = 'crm_guest',
    $primaryKey = 'Id_Guest';
    
    public $timestamps = false;
}
