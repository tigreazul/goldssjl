<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuario',
    $primaryKey = 'idusuario';
    public $timestamps = false;
}
