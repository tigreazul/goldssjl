<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
    protected $table = 'tbl_distrito',
    $primaryKey = 'pk_dist_int_id';
    public $timestamps = false;
}
