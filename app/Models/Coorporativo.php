<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coorporativo extends Model
{
    protected $table = 'coorporativo',
    $primaryKey = 'codigo';
    public $timestamps = false;
}
