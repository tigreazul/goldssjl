<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use BD;
use App\Models\Acuerdo;
use DB;

use Funciones;
use Json;

class ContratoController extends Controller
{

	public function index(Request $request,$id){
		$cod_usuario = \DB::table('usuario')
			->join('empleado', 'usuario.idempleado', '=', 'empleado.idempleado')
		    ->where(['usuario.idusuario'=>$id,'usuario.estado'=>'A','usuario.sesion_activo'=>1])
		    ->first();
            // dd($cod_usuario);
		if(!$cod_usuario){
			// return redirect('http://localhost:8888/gym/');	
		}

        $fecha_actual = date('Y-m-d H:i:s');
        $acuerdo = DB::select("SELECT     
            nroacuerdo as Acuerdo,    
            c_cliente as Codigo,    
             (select rtrim(nombre) + ' ' + rtrim(apelpat) + ' ' + rtrim(apelmat) from cliente where codigo_local=acuerdos.codigo_local and c_cliente=acuerdos.c_cliente) as Cliente,    
            rtrim(membresia) as membresia,    
            fecinicio,    
            fecfin,    
            feccontrato,    
            fechaimpresion,    
            usuario as Usuario,estado,registro_ac,c_cliente,    
            case estado when 'AN' then 'ANULADO' when 'DS' then 'DESCARTADO' else 'IMPRESO' end estadoocu,    
            detalle,    
            case estado when 'AN' then fechaanulacion + ' ' + fechaanulacion else '' end fecestado,    
            usuarioanulado,codigo idacuerdo from acuerdos    
            where     
            codigo_local=15 and fechaimpresion BETWEEN '2018-12-20' AND '".$fecha_actual."'");


		// $acuerdo = Acuerdo::whereBetween('FechaImpresion', ['2018-12-20', $fecha_actual])->get();
        // dd($acuerdo);

		return view('contrato.index')
        ->with('menu','')
        ->with('acuerdo',$acuerdo)
        ->with('usuario',$cod_usuario)
        ->with('username',utf8_decode($cod_usuario->apellidos." ".$cod_usuario->nombre))
        ->with('title','Lista Contrato');
	}

	public function store(Request $request){
        $data = $request->validate([
            'prospecto'     => 'required',
            'procedencia'   => 'required',
            'subprocedencia'=> 'required',
            'corporativo'   => 'required',
            'campania'      => 'required',
            'nro_hoja'      => 'required',
            'fitness'       => 'required',
            'documento'     => 'required',
            'nro_documento' => 'required',
            'nombre'       	=> 'required',
            'ap_paterno'    => 'required',
            'ap_materno'    => 'required',
            'direccion'     => 'required',
            'fnacimiento'   => 'required',
            'sexo'       	=> 'required',
            'ecivil'   		=> 'required',
            'telefono'   	=> 'required',
            'email'   		=> 'required',
            'parentesco'   	=> 'required',
            'ocupacion'   	=> 'required',
            'hijos'   		=> 'required',
            'distrito'   	=> 'required',
            'provincia'   	=> 'required',
            'departamento'  => 'required',
            'membresia'  => '',
            'nacionalidad'  => 'required'
        ], [
            'prospecto.required'        => 'El campo prospecto es obligatorio',
            'procedencia.required'      => 'El campo procedencia es obligatorio',
            'subprocedencia.required'   => 'El campo subprocedencia es obligatorio',
            'corporativo.required'      => 'El campo corporativo es obligatorio',
            'campania.required'      	=> 'El campo campania es obligatorio',
            'nro_hoja.required'      	=> 'El campo nro_hoja es obligatorio',
            'fitness.required'  		=> 'El campo fitness es obligatorio',
            'documento.required'  		=> 'El campo documento es obligatorio',
            'nro_documento.required'  	=> 'El campo nro_documento es obligatorio',
            'nombre.required'  			=> 'El campo nombre es obligatorio',
            'ap_paterno.required'  		=> 'El campo ap_paterno es obligatorio',
            'ap_materno.required'  		=> 'El campo ap_materno es obligatorio',
            'direccion.required'  		=> 'El campo direccion es obligatorio',
            'fnacimiento.required'  	=> 'El campo fnacimiento es obligatorio',
            'sexo.required'  			=> 'El campo sexo es obligatorio',
            'ecivil.required'  			=> 'El campo ecivil es obligatorio',
            'telefono.required'  		=> 'El campo telefono es obligatorio',
            'email.required'  			=> 'El campo email es obligatorio',
            'parentesco.required'  		=> 'El campo parentesco es obligatorio',
            'ocupacion.required'  		=> 'El campo ocupacion es obligatorio',
            'hijos.required'  			=> 'El campo hijos es obligatorio',
            'distrito.required'  		=> 'El campo distrito es obligatorio',
            'provincia.required'  		=> 'El campo provincia es obligatorio',
            'departamento.required'  	=> 'El campo departamento es obligatorio',
            'nacionalidad.required'  	=> 'El campo nacionalidad es obligatorio'
        ]);

        
        $persona = new Persona;
        $persona->codigo_prospecto  		= $data['prospecto'];

        $persona->procedencia       		= $data['procedencia'];
        $persona->subprocedencia    		= $data['subprocedencia'];
        $persona->corporativo       		= $data['corporativo'];
        $persona->campania        			= $data['campania'];
        $persona->nro_hoja_atencion 		= $data['nro_hoja'];
        $persona->fitness        			= $data['fitness'];
        $persona->tipo_documento    		= $data['documento'];
        $persona->num_documento     		= $data['nro_documento'];
        $persona->nombre        			= $data['nombre'];
        $persona->apellido_paterno  		= $data['ap_paterno'];
        $persona->apellido_materno  		= $data['ap_materno'];
        $persona->direccion_calle   		= $data['direccion'];
        $persona->fecha_nacimiento  		= $data['fnacimiento'];
        $persona->sexo        				= $data['sexo'];
        $persona->estado_civil      		= $data['ecivil'];
        $persona->telefono        			= $data['telefono'];
        $persona->email        				= $data['email'];
        $persona->parentesco        		= $data['parentesco'];
        $persona->ocupacion        			= $data['ocupacion'];
        $persona->hijos        				= $data['hijos'];
        $persona->idmembresia       		= $data['membresia'];
        $persona->direccion_distrito        = $data['distrito'];
        $persona->direccion_provincia       = $data['provincia'];
        $persona->direccion_departamento    = $data['departamento'];
        $persona->nacionalidad      		= $data['nacionalidad'];
        $persona->fecha_registro    		= date('Y-m-d');
        $persona->estado            		= 'A';
        $persona->usuario_creacion  		= 'jtello';
        $persona->tipo_persona      		= 'Cliente';
        $persona->save();

        // \Session::flash('success', 'Se registro correctamente el evento');
        return redirect()->route('prospecto.inicio');
    }



}