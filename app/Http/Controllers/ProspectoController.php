<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use BD;

use App\Models\Maestro;

use App\Models\Departamento;
use App\Models\Provincia;
use App\Models\Distrito;
use App\Models\Persona;
use App\Models\Nota;
use App\Models\Usuario;
use App\Models\Articulo;
use App\Models\Prospecto;
use App\Models\Empleado;
use App\Models\procedencia;
use App\Models\Subprocedencia;
use App\Models\Coorporativo;

use App\Models\Tipodocumento;
use App\Models\Campania;
use App\Models\Estadocivil;
use App\Models\Parentesco;
use App\Models\Ocupacion;
use App\Models\Pais;
use DB;

use Funciones;
use Json;

class ProspectoController extends Controller
{

	public function index(Request $request,$id){
		$cod_usuario = \DB::table('usuario')
			->join('empleado', 'usuario.idempleado', '=', 'empleado.idempleado')
		    ->where(['usuario.idusuario'=>$id,'usuario.estado'=>'A','usuario.sesion_activo'=>1])
		    ->first();
		// dd($cod_usuario);

		if(!$cod_usuario){
			// return redirect('http://localhost:8888/gym/');	
		}


		// $cod_usuario = \DB::table('usuario')
		// 	->join('empleado', 'usuario.idempleado', '=', 'empleado.idempleado')			
		// 	->join('prospecto', 'empleado.idempleado', '=', 'prospecto.idprospecto')
		//     ->where(['usuario.idusuario'=>$id,'usuario.estado'=>'A','usuario.sesion_activo'=>1])
		//     ->first();

		$procedencia 	= procedencia::where(['estado'=>1])->orderBy('idProcedencia','DESC')->get();
		$subprocedencia = Subprocedencia::where(['ESTADO'=>1])->orderBy('IdSubProcedencia','DESC')->get();
		$fitness 		= Maestro::where(['ESTADO'=>1,'HIJO'=>3])->orderBy('ID','DESC')->get();
		$corporativo 	= Coorporativo::where(['estado'=>1])->orderBy('codigo','DESC')->get();
		$campania 		= Campania::where(['estado'=>1])->orderBy('idCampana','DESC')->get();
		$tdocument 		= Tipodocumento::where(['ESTADO'=>1])->orderBy('idtipo_documento','DESC')->get();
		$ecivil 		= Estadocivil::where(['estado'=>1])->orderBy('idEstadoCivil','DESC')->get();
		$parentesco		= Parentesco::where(['estado'=>1])->orderBy('idParentesco','DESC')->get();
		$ocupacion		= Ocupacion::where(['estado'=>1])->orderBy('idOcupacion','DESC')->get();
		$nacionalidad	= Pais::where(['estado'=>1])->orderBy('idPais','DESC')->get();
		// dd($corporativo);
		
		$sexo 			= Maestro::where(['ESTADO'=>1,'HIJO'=>6])->orderBy('ID','DESC')->get();
		$hijos 			= Maestro::where(['ESTADO'=>1,'HIJO'=>9])->orderBy('ID','DESC')->get();
		$calificacion	= Maestro::where(['ESTADO'=>1,'HIJO'=>13])->orderBy('ID','DESC')->get();
		
		$departamento	= Departamento::where(['estado'=>1])->orderBy('descripcion','DESC')->get();
		$membresia		= Articulo::where(['estado'=>'A'])->orderBy('descripcion','DESC')->get();
        
        // $prospecto		= Persona::where(['Estado'=>'A'])->orderBy('idpersona','DESC')->get();
        // $prospecto		= Prospecto::where(['Estado'=>'A'])->orderBy('idprospecto','DESC')->get();
        // $prospecto      = Prospecto::where(['Estado'=>'1'])->orderBy('ApelPat','DESC')->orderBy('ApelMat','DESC')->get();
        $prospecto		= Prospecto::orderBy('ApelPat','DESC')->orderBy('ApelMat','DESC')->get();
        // dd($prospecto);
        
        $nota           = Nota::where(['usuario_creacion'=>'','estado'=>'1'])->orderBy('idnota','DESC')->get();

		return view('prospecto.index')
        ->with('menu','')
        ->with('procedencia',$procedencia)
        ->with('subprocedencia',$subprocedencia)
        ->with('corporativo',$corporativo)
        ->with('campania',$campania)
        ->with('sexo',$sexo)
        ->with('ecivil',$ecivil)
        ->with('tdocument',$tdocument)
        ->with('ocupacion',$ocupacion)
        ->with('hijos',$hijos)
        ->with('parentesco',$parentesco)
        ->with('nacionalidad',$nacionalidad)
        ->with('calificacion',$calificacion)
        ->with('departamento',$departamento)
        ->with('persona',$prospecto)
        ->with('fitness',$fitness)
        ->with('notas',$nota)
        ->with('membresia',$membresia)
        ->with('usuario',$cod_usuario)
        ->with('username',utf8_decode($cod_usuario->apellidos." ".$cod_usuario->nombre))
        ->with('title','Lista');
	}

	public function store(Request $request){
        $data = $request->validate([
            'prospecto'     => 'required',
            'procedencia'   => 'required',
            'subprocedencia'=> 'required',
            'corporativo'   => 'required',
            'campania'      => 'required',
            'nro_hoja'      => 'required',
            'fitness'       => 'required',
            'documento'     => 'required',
            'nro_documento' => 'required',
            'nombre'       	=> 'required',
            'ap_paterno'    => 'required',
            'ap_materno'    => 'required',
            'direccion'     => 'required',
            'fnacimiento'   => 'required',
            'sexo'       	=> 'required',
            'ecivil'   		=> 'required',
            'telefono'   	=> 'required',
            'email'   		=> 'required',
            'parentesco'   	=> 'required',
            'ocupacion'   	=> 'required',
            'hijos'   		=> 'required',
            'distrito'   	=> 'required',
            'provincia'   	=> 'required',
            'departamento'  => 'required',
            'membresia'  => '',
            'nacionalidad'  => 'required'
        ], [
            'prospecto.required'        => 'El campo prospecto es obligatorio',
            'procedencia.required'      => 'El campo procedencia es obligatorio',
            'subprocedencia.required'   => 'El campo subprocedencia es obligatorio',
            'corporativo.required'      => 'El campo corporativo es obligatorio',
            'campania.required'      	=> 'El campo campania es obligatorio',
            'nro_hoja.required'      	=> 'El campo nro_hoja es obligatorio',
            'fitness.required'  		=> 'El campo fitness es obligatorio',
            'documento.required'  		=> 'El campo documento es obligatorio',
            'nro_documento.required'  	=> 'El campo nro_documento es obligatorio',
            'nombre.required'  			=> 'El campo nombre es obligatorio',
            'ap_paterno.required'  		=> 'El campo ap_paterno es obligatorio',
            'ap_materno.required'  		=> 'El campo ap_materno es obligatorio',
            'direccion.required'  		=> 'El campo direccion es obligatorio',
            'fnacimiento.required'  	=> 'El campo fnacimiento es obligatorio',
            'sexo.required'  			=> 'El campo sexo es obligatorio',
            'ecivil.required'  			=> 'El campo ecivil es obligatorio',
            'telefono.required'  		=> 'El campo telefono es obligatorio',
            'email.required'  			=> 'El campo email es obligatorio',
            'parentesco.required'  		=> 'El campo parentesco es obligatorio',
            'ocupacion.required'  		=> 'El campo ocupacion es obligatorio',
            'hijos.required'  			=> 'El campo hijos es obligatorio',
            'distrito.required'  		=> 'El campo distrito es obligatorio',
            'provincia.required'  		=> 'El campo provincia es obligatorio',
            'departamento.required'  	=> 'El campo departamento es obligatorio',
            'nacionalidad.required'  	=> 'El campo nacionalidad es obligatorio'
        ]);

        
        $persona = new Persona;
        $persona->codigo_prospecto  		= $data['prospecto'];

        $persona->procedencia       		= $data['procedencia'];
        $persona->subprocedencia    		= $data['subprocedencia'];
        $persona->corporativo       		= $data['corporativo'];
        $persona->campania        			= $data['campania'];
        $persona->nro_hoja_atencion 		= $data['nro_hoja'];
        $persona->fitness        			= $data['fitness'];
        $persona->tipo_documento    		= $data['documento'];
        $persona->num_documento     		= $data['nro_documento'];
        $persona->nombre        			= $data['nombre'];
        $persona->apellido_paterno  		= $data['ap_paterno'];
        $persona->apellido_materno  		= $data['ap_materno'];
        $persona->direccion_calle   		= $data['direccion'];
        $persona->fecha_nacimiento  		= $data['fnacimiento'];
        $persona->sexo        				= $data['sexo'];
        $persona->estado_civil      		= $data['ecivil'];
        $persona->telefono        			= $data['telefono'];
        $persona->email        				= $data['email'];
        $persona->parentesco        		= $data['parentesco'];
        $persona->ocupacion        			= $data['ocupacion'];
        $persona->hijos        				= $data['hijos'];
        $persona->idmembresia       		= $data['membresia'];
        $persona->direccion_distrito        = $data['distrito'];
        $persona->direccion_provincia       = $data['provincia'];
        $persona->direccion_departamento    = $data['departamento'];
        $persona->nacionalidad      		= $data['nacionalidad'];
        $persona->fecha_registro    		= date('Y-m-d');
        $persona->estado            		= 'A';
        $persona->usuario_creacion  		= 'jtello';
        $persona->tipo_persona      		= 'Cliente';
        $persona->save();

        // \Session::flash('success', 'Se registro correctamente el evento');
        return redirect()->route('prospecto.inicio');
    }

    public function get_provincia_id(Request $request,$id){
        $listado = Provincia::where(['depId'=>$id,'estado'=>1])->orderBy('provID','DESC')->get();
        if ( !$listado  )
        {
            Json::setMessage('ID invalido.');
        }
        if ( $listado )
        {
	        Json::setStatus('ok');
	        Json::setData($listado);
	       	Json::setMessage('Listado correctamente.');
        }else{
            Json::setMessage('Intentelo nuevamente.');	
        }
        echo Json::getJson();
    }

    public function get_distrito_id(Request $request,$id){
        $listado = Distrito::where(['provId'=>$id,'estado'=>1])->orderBy('distID','DESC')->get();
        if ( !$listado  )
        {
            Json::setMessage('ID invalido.');
        }
        if ( $listado )
        {
	        Json::setStatus('ok');
	        Json::setData($listado);
	       	Json::setMessage('Listado correctamente.');
        }else{
            Json::setMessage('Intentelo nuevamente.');	
        }
        echo Json::getJson();
    }

    public function getconsulta(Request $request,$id){
    	// $listado = Prospecto::where(['Id_Guest'=>$id,'estado'=>1])->first();
        $listado = DB::select('call obtenerProspecto(?, ?)',[$id,15])[0];

        if ( !$listado  )
        {
            Json::setMessage('ID invalido.');
        }
        if ( $listado )
        {
	        Json::setStatus('ok');
	        Json::setData($listado);
	       	Json::setMessage('Listado correctamente.');
        }else{
            Json::setMessage('Intentelo nuevamente.');	
        }
        echo Json::getJson();
    }

    public function validDocumento(Request $request,$dni){
        //$listado = Persona::where(['num_documento'=>$dni,'estado'=>'A'])->orderBy('num_documento','DESC')->first();
    	$listado = Prospecto::where(['DNI'=>$dni])->first();
        if ( !$listado  )
        {
            Json::setMessage('ID invalido.');
        }
        if ( $listado )
        {
	        Json::setStatus('ok');
	        Json::setData($listado);
	       	Json::setMessage('Listado correctamente.');
        }else{
            Json::setMessage('Intentelo nuevamente.');	
        }
        echo Json::getJson();
    }

    public function addnota(Request $request){
        // dd($request);
        $data = $request->validate([
            'asunto'       => 'required',
            'mensaje'      => 'required',
            'calificacion' => 'required',
            'persoid'      => 'required'
        ], [
            'asunto.required'       => 'El campo asunto es obligatorio',
            'mensaje.required'      => 'El campo mensaje inicio es obligatorio',
            'calificacion.required' => 'El campo calificación es obligatorio',
            'persoid.required'      => 'El campo persona es obligatorio'
        ]);
        
        $nota = new Nota;
        $nota->asunto           = $data['asunto'];
        $nota->informacion      = $data['mensaje'];
        $nota->calificacion     = $data['calificacion'];
        $nota->idpersona        = $data['persoid'];
        $nota->usuario_creacion = 'jtello';
        $nota->fecha_registro   = date('Y-m-d');
        $nota->fecha_seguimiento= date('Y-m-d');
        $nota->estado           = 1;
        $nota->save();
        
        if($nota->idnota){
        	$listado = Nota::where(['usuario_creacion'=>'jtello','estado'=>'1'])->orderBy('idnota','DESC')->get();
            // $listado = Nota::where(['idnota'=>$nota->idnota,'estado'=>'1'])->orderBy('idnota','DESC')->get();
            Json::setStatus('ok');
            Json::setData($listado);
            Json::setMessage('');	
        }
        echo Json::getJson();
        // return redirect()->route('calendar.inicio');
    }

    public function dataProspecto(Request $request){
        ini_set('memory_limit', '-1');
        $params = $columns = $totalRecords = $data = array();
        $where = $sqlTot = $sqlRec = "";
        // dd($_POST); die();
        $params = $_POST;

        // print_r($params);// die();

        $columns = array( 
            0 =>'Id_Guest',
            1 =>'ApelMat', 
            2 => 'Apelpat',
            3 => 'DNI'
        );

        
        
        if( !empty($params['search']['value']) ) {   
            $where .=" WHERE ";
            $where .=" ( Nombres LIKE '".$params['search']['value']."%' ";    
            $where .=" OR ApelMat LIKE '".$params['search']['value']."%' ";
            $where .=" OR Apelpat LIKE '".$params['search']['value']."%' ";
            $where .=" OR DNI LIKE '".$params['search']['value']."%' ";
        }
        $sql = "SELECT Id_Guest,ApelMat,Apelpat,DNI FROM `crm_guest` ";
        $sqlTot .= $sql;
        $sqlRec .= $sql;
        //concatenate search sql if value exist
        if(isset($where) && $where != '') {

            $sqlTot .= $where;
            $sqlRec .= $where;
        }

        // echo $columns[$params['order'][0]['column']]; die();
        $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

        // $total = DB::select("$sqlRec")->count();
        $total = Prospecto::count();

        // print_r($total); die();

        $results = DB::select("$sqlRec");

        foreach ($results as $row) {
            $data[] = array(
                '<a href="#" class="btn btn-warning seleccion" data-id="'.$row->Id_Guest.'" class="selProspecto"><i class="fa fa-check"></i></a>',
                $row->ApelMat,
                $row->Apelpat,
                $row->DNI
            );
        }
        


        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $total ),  
            "recordsFiltered" => intval( $total ),
            "data"            => $data   // total data array
            );

        // print_r($json_data); die();

        echo json_encode($json_data); 
        
        // echo $sqlRec."----";
        // echo $sqlTot;

    }

}