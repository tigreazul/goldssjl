<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Maestro;

use App\Models\Departamento;
use App\Models\Provincia;
use App\Models\Distrito;
use App\Models\Articulo;
use App\Models\Persona;
use App\Models\Nota;
use Funciones;
use Json;

class TransferenciaController extends Controller
{

    public function index(Request $requies,$id){
        $cod_usuario = \DB::table('usuario')
            ->join('empleado', 'usuario.idempleado', '=', 'empleado.idempleado')
            ->where(['usuario.idusuario'=>22,'usuario.estado'=>'A','usuario.sesion_activo'=>1])
            ->first();

        // if(!$cod_usuario){
        //     return redirect('http://localhost:8888/gym/');  
        // }

        $persona        = Persona::where(['estado'=>'A','idpersona'=>$id])->orderBy('idpersona','DESC')->first();
        if(!$persona){
            \Session::flash('message', 'No tiene autorizacion para ingresar al modulo');
            return redirect()->route('login.ldap');
        }

        $procedencia    = Maestro::where(['TITULO'=>$persona->procedencia,'ESTADO'=>1,'HIJO'=>0])->orderBy('ID','DESC')->get();
        $subprocedencia = Maestro::where(['TITULO'=>$persona->subprocedencia,'ESTADO'=>1,'HIJO'=>1])->orderBy('ID','DESC')->get();
        $fitness        = Maestro::where(['TITULO'=>$persona->fitness,'ESTADO'=>1,'HIJO'=>3])->orderBy('ID','DESC')->get();
        $corporativo    = Maestro::where(['TITULO'=>$persona->corporativo,'ESTADO'=>1,'HIJO'=>4])->orderBy('ID','DESC')->get();
        $campania       = Maestro::where(['TITULO'=>$persona->campania,'ESTADO'=>1,'HIJO'=>5])->orderBy('ID','DESC')->get();
        $sexo           = Maestro::where(['TITULO'=>$persona->sexo,'ESTADO'=>1,'HIJO'=>6])->orderBy('ID','DESC')->get();
        $ecivil         = Maestro::where(['TITULO'=>$persona->estado_civil,'ESTADO'=>1,'HIJO'=>7])->orderBy('ID','DESC')->get();
        $tdocument      = Maestro::where(['TITULO'=>$persona->tipo_documento,'ESTADO'=>1,'HIJO'=>8])->orderBy('ID','DESC')->get();
        $hijos          = Maestro::where(['TITULO'=>$persona->hijos,'ESTADO'=>1,'HIJO'=>9])->orderBy('ID','DESC')->get();
        $ocupacion      = Maestro::where(['TITULO'=>$persona->ocupacion,'ESTADO'=>1,'HIJO'=>10])->orderBy('ID','DESC')->get();
        $parentesco     = Maestro::where(['TITULO'=>$persona->parentesco,'ESTADO'=>1,'HIJO'=>11])->orderBy('ID','DESC')->get();
        $nacionalidad   = Maestro::where(['TITULO'=>$persona->nacionalidad,'ESTADO'=>1,'HIJO'=>12])->orderBy('ID','DESC')->get();
        $calificacion   = Maestro::where(['TITULO'=>$persona->procedencia,'ESTADO'=>1,'HIJO'=>13])->orderBy('ID','DESC')->get();
        
        $departamento   = Departamento::where(['descripcion'=>$persona->direccion_departamento,'estado'=>1])->orderBy('descripcion','DESC')->get();
        $provincia      = Provincia::where(['descripcion'=>$persona->direccion_provincia,'estado'=>1])->orderBy('descripcion','DESC')->get();
        $distrito       = Distrito::where(['descripcion'=>$persona->direccion_distrito,'estado'=>1])->orderBy('descripcion','DESC')->get();

        $membresia      = Articulo::where(['idarticulo'=>$persona->idmembresia,'estado'=>'A'])->orderBy('descripcion','DESC')->get();
        $nota           = Nota::where(['idpersona'=>$persona->idpersona,'estado'=>'1'])->orderBy('idnota','DESC')->get();
        // dd($membresia);
        return view('transferido.inx_transf')
        ->with('menu','')
        ->with('procedencia',$procedencia)
        ->with('subprocedencia',$subprocedencia)
        ->with('corporativo',$corporativo)
        ->with('campania',$campania)
        ->with('sexo',$sexo)
        ->with('ecivil',$ecivil)
        ->with('tdocument',$tdocument)
        ->with('ocupacion',$ocupacion)
        ->with('hijos',$hijos)
        ->with('parentesco',$parentesco)
        ->with('nacionalidad',$nacionalidad)
        ->with('calificacion',$calificacion)
        ->with('departamento',$departamento)
        ->with('persona',$persona)
        ->with('fitness',$fitness)
        ->with('notas',$nota)
        ->with('membresia',$membresia)
        ->with('usuario',$cod_usuario)
        ->with('username',utf8_decode($cod_usuario->apellidos." ".$cod_usuario->nombre))
        ->with('title','Lista de compras');


	}
}