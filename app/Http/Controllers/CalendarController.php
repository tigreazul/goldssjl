<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Calendar;
use Funciones;
use Json;

class CalendarController extends Controller
{
 
    public function index(){
        if(Funciones::sesiones() === 'false') return redirect()->route('login.ldap')->with('success',true);

        $modulo = \Session::get('uModule');
        $calendario = Calendar::where('estado',1)->orderBy('ID','DESC')->get();
        $username = \Session::get('uName');
        return view('calendar.index')
        ->with('calendario',$calendario)
        ->with('modulo',$modulo)
        ->with('menu','')
        ->with('username',$username)
        ->with('title','Lista de compras');
    }

    public function lista_calendar(){
        $listado = Calendar::where('estado',1)->orderBy('ID','DESC')->get();
        $msg = array(
            "#f56954", // Rojo
            "#f39c12", // Naranja
            "#3c8dbc",
            '#00a65a',
            '#00c0ef'
            );
        $todo = (count($msg)-1);
        if ( $listado )
        {
            $ar = array();
            foreach ($listado as $lst) {
                $randon = rand(0,$todo);
                $ar[] = array(
                    'id' 	=> $lst->ID,
                    'title' => $lst->TITULO,
                    'start' => \Carbon\Carbon::parse($lst->FECHA_INICIO)->format('Y-m-d')." ".$lst->HORA_INICIO,
                    'end' 	=> \Carbon\Carbon::parse($lst->FECHA_FIN)->format('Y-m-d')." ".$lst->HORA_FIN,
                    'backgroundColor' => $lst->COLOR,
                    'borderColor' => $lst->COLOR
                    // 'backgroundColor' => $msg[$randon],
                    // 'borderColor' => $msg[$randon]
                    );
            }
            echo json_encode($ar);
        }
    }

    public function get_dataid(Request $request){
        // ID de Formato        
        $id = request()->post('id');
        $listado = Calendar::where(['ID'=>$id,'ESTADO'=>1])->orderBy('ID','DESC')->first();
        // Valida que el formato exista
        if ( !$listado  )
        {
            Json::setMessage('ID invalido.');
        }
        if ( $listado )
        {
            Json::setStatus('ok');
            Json::setItem('id',$listado->ID);
            Json::setItem('titulo',$listado->TITULO);
            Json::setItem('description',$listado->DESCRIPCION);
            Json::setItem('fecha_inicio', \Carbon\Carbon::parse($listado->FECHA_INICIO)->format('Y-m-d') );
            Json::setItem('fecha_fin',\Carbon\Carbon::parse($listado->FECHA_FIN)->format('Y-m-d'));
            Json::setItem('hora_inicio',$listado->HORA_INICIO);
            Json::setItem('hora_fin',$listado->HORA_FIN);
            Json::setItem('cargo',$listado->CARGO);
            Json::setItem('color',$listado->COLOR);
            Json::setMessage('Listado correctamente.');	
        }else{
            Json::setMessage('Intentelo nuevamente.');	
        }
        echo Json::getJson();
    }
    
    public function store(Request $request){
        // dd('ss');
        $data = $request->validate([
            'cargo'       => 'required',
            'iniDate'       => 'required',
            'titulo'        => 'required',
            'iniHora'       => 'required',
            'finDate'       => 'required',
            'finTime'       => 'required',
            'description'   => 'required'
        ], [
            'cargo.required'        => 'El campo cargo es obligatorio',
            'iniDate.required'      => 'El campo fecha inicio es obligatorio',
            'titulo.required'       => 'El campo titulo es obligatorio',
            'iniHora.required'      => 'El campo hora de inicio es obligatorio',
            'finDate.required'      => 'El campo fecha de fin es obligatorio',
            'finTime.required'      => 'El campo hora de fin es obligatorio',
            'description.required'  => 'El campo descripcion es obligatorio'
        ]);

        
        $calendar = new Calendar;
        $calendar->TITULO           = $data['titulo'];
        $calendar->CARGO            = $data['cargo'];
        $calendar->DESCRIPCION      = $data['description'];
        $calendar->FECHA_INICIO     = $data['iniDate'];
        $calendar->FECHA_FIN        = $data['finDate'];
        if($data['cargo'] == 1){
            $calendar->COLOR        = '#f56954';
        }else{
            $calendar->COLOR       = '#f39c12';
        }
        $calendar->FECHA_REGISTRO   = date('Y-m-d');
        $calendar->ESTADO           = 1;
        $calendar->HORA_INICIO      = $data['iniHora'];
        $calendar->HORA_FIN         = $data['finTime'];
        $calendar->save();
        \Session::flash('success', 'Se registro correctamente el evento');
        return redirect()->route('calendar.inicio');
    }

    public function delete(Request $request){
        $id = request()->post('d');
        $listado = Calendar::where(['ID'=>$id,'ESTADO'=>1])->orderBy('ID','DESC')->first();
        if ( !$listado )
        {
            Json::setMessage('ID invalido.');
        }

        $calendar = Calendar::find($id);
        $calendar->ESTADO             = 0;
        $calendar->FECHA_MODIFICACION = date('Y-m-d H:i:s');
        $calendar->save();

        if($calendar->ID){
            Json::setStatus('ok');
            Json::setMessage('Se ha eliminado el evento.');	
            \Session::flash('success', 'Se ha eliminado el evento');
        }
        echo Json::getJson();
    }

    public function update(Request $request,$id){
        $data = $request->validate([
            'cargo'         => 'required',
            'iniDate'       => 'required',
            'titulo'        => 'required',
            'iniHora'       => 'required',
            'finDate'       => 'required',
            'finTime'       => 'required',
            'description'   => 'required'
        ], [
            'cargo.required'        => 'El campo cargo es obligatorio',
            'iniDate.required'      => 'El campo fecha inicio es obligatorio',
            'titulo.required'       => 'El campo titulo es obligatorio',
            'iniHora.required'      => 'El campo hora de inicio es obligatorio',
            'finDate.required'      => 'El campo fecha de fin es obligatorio',
            'finTime.required'      => 'El campo hora de fin es obligatorio',
            'description.required'  => 'El campo descripcion es obligatorio'
        ]);

        $listado = Calendar::where(['ID'=>$id,'ESTADO'=>1])->orderBy('ID','DESC')->first();
        if ( !$listado )
        {
            Json::setMessage('ID invalido.');
        }

        $calendar = Calendar::find($id);
        $calendar->TITULO           = $data['titulo'];
        $calendar->DESCRIPCION      = $data['description'];
        $calendar->FECHA_INICIO     = $data['iniDate'];
        $calendar->FECHA_FIN        = $data['finDate'];
        $calendar->CARGO            = $data['cargo'];
        if($data['cargo'] == 1){
            $calendar->COLOR        = '#f56954';
        }else{
            $calendar->COLOR        = '#f39c12';
        }
        $calendar->HORA_INICIO      = $data['iniHora'];
        $calendar->HORA_FIN         = $data['finTime'];
        $calendar->save();

        if($calendar->ID){
            Json::setStatus('ok');
            Json::setMessage('Se ha actualizado el evento.');	
            \Session::flash('success', 'Se ha actualizado el evento');
        }
        echo Json::getJson();
    }

    public function user(){
        return view('calendar.calendar_user')
        ->with('title','Lista Calendario');
    }

}