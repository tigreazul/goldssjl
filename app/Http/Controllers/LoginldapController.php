<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proveedor;
use App\Models\Usuario;
use Funciones;

class LoginldapController extends Controller
{

    public function index()
    {
        if (\Session::has('userId')){
            return redirect()->route('compra.inicio')->with('success',true);
        }else{
            return view('login.login')
            ->with('menu','')
            ->with('title','Registro de proveedores');
        }
    }

    public function valid(Request $request){
        // email
        $data = $request->validate([
            'email'       => 'required',
            'password'    => 'required'
        ], [
            'email.required'    => 'El campo Email es obligatorio',
            'password.required' => 'El campo Contraseña es obligatorio',
        ]);
        
        $user =  $data['email'];
        $pwd  =  $data['password'];
        $server = env('LDAP_DOMINIO', ''); // @sbn.gob.pe
        
        $ldapserver = env('LDAP_CONNECTION', ''); // 192.168.5.1

        $ldapuser   = $user.$server;  
        $ldappass   = $pwd;
        $ldaptree   = env('LDAP_BASEDN', ''); // CN=Person,CN=Schema,CN=Configuration,DC=sbn,DC=gob,DC=pe
        $ldapconn = ldap_connect($ldapserver) or die("Could not connect to LDAP server.");
        if($ldapconn) {
            $ldapbind = @ldap_bind($ldapconn, $ldapuser, $ldappass);// return ("Error trying to bind: ".ldap_error($ldapconn));
            if ($ldapbind) {
                $mUser = Usuario::where(['USERID'=>$user,'CORREO'=>$ldapuser,'Estado'=>1,'AUTORIZACION'=>1])->first();
                if(!$mUser){
                    \Session::flash('message', 'No tiene autorizacion para ingresar al modulo');
                    return redirect()->route('login.ldap');
                }
                \Session::put('userId',$mUser->USERID);
                \Session::put('uEmail',$mUser->CORREO);
                \Session::put('uModule',$mUser->MODULO);
                \Session::put('uName',$mUser->NAME);
                
                return redirect()->route('calendar.inicio');
                // if($mUser->MODULO == 1){
                // }else{
                //     return redirect()->route('compra.enviados')->with('success',true);
                // }
            } else {
                \Session::flash('message', 'Usuario y/o contraseña incorrecto');
                return redirect()->route('login.ldap');
            }
        }
        ldap_close($ldapconn);
    }

    public function logout(){
        if( \Session::has('pwd') == true){
            \Session::flush();
            return redirect()->route('announcement.listado');
        }else{
            \Session::flush();
            return redirect()->route('login.ldap');
        }
    }
}