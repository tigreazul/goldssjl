<?php
use League\Flysystem\Filesystem;
use App\Models\Proveedor;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/' ,'CalendarController@user')->name('calendar.user');

// Login inicio de sesion
Route::get('admin' ,'LoginldapController@index')->name('login.ldap');
Route::post('ldap' ,'LoginldapController@valid')->name('login.iniciar');
Route::get('logout' ,'LoginldapController@logout')->name('login.logout');


// Route::group(['middleware' => 'web'], function () {
	// Prospecto
	Route::get('/prospecto/{id}' ,'ProspectoController@index')->name('prospecto.inicio');
	// Route::get('/prospetocs/{$id}' ,'ProspectoController@index')->name('prospecto.inicio');
	Route::get('provincia/{id}' ,'ProspectoController@get_provincia_id')->name('ubigeo.provincia');
	Route::get('distrito/{id}' ,'ProspectoController@get_distrito_id')->name('ubigeo.distrito');
	Route::post('prospecto' ,'ProspectoController@store')->name('prospecto.registro');
	Route::get('getconsulta/{id}' ,'ProspectoController@getconsulta')->name('prospecto.getprospectoid');
	
	Route::post('data-prospecto' ,'ProspectoController@dataProspecto')->name('prospecto.dataProspecto');
	
	Route::get('valid-documento/{dni}' ,'ProspectoController@validDocumento')->name('prospecto.documento');
	Route::post('nota' ,'ProspectoController@addnota')->name('prospecto.nota');
	// Transferencia
	Route::get('transferencia/{id}' ,'TransferenciaController@index')->name('transferencia.inicio');
	Route::get('getTransferencia/{id}' ,'TransferenciaController@index')->name('transferencia.inicio');

	Route::get('contrato-cliente/{id}' ,'ContratoController@index')->name('contrato.inicio');
// });

