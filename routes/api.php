<?php

use Illuminate\Http\Request;
use App\Models\Proveedor;
use App\Models\Convocatoria;
use App\Models\ConvocatoriaProveedor;
use App\Models\ObservacionRequerimiento;
use App\Models\Buzon;
use Illuminate\Support\Facades\Input;
// use Funciones;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('xxxx',function(){
    $acuerdo =  \DB::select('SELECT * FROM tAcuerdos');
    // dd($acuerdo);
    foreach ($acuerdo as $value) {
    	$detalle 		= ($value->Detalle!='')?$value->Detalle:'NULL';
    	$FechaAnulacion = ($value->FechaAnulacion!='')?$value->FechaAnulacion:'NULL';
    	$UsuarioAnulado = ($value->UsuarioAnulado!='')?$value->UsuarioAnulado:'NULL';
    	$PcAnulado 		= ($value->PcAnulado!='')?$value->PcAnulado:'NULL';
    	
    	// 10/05/2015 06:47:06 PM
    	$FechaImpresion = $value->FechaImpresion;
    	$FechaImpresion1 = explode(' ', $FechaImpresion);
    	$FechaImpresion2 = explode('/', $FechaImpresion1[0]);
    	// dd($FechaImpresion);
    	$FecContrato = explode('/', $value->FecContrato);
    	$FecContrato = $FecContrato[2].'/'.$FecContrato[1].'/'.$FecContrato[0];

    	$FecInicio = explode('/', $value->FecInicio);
    	$FecInicio = $FecInicio[2].'/'.$FecInicio[1].'/'.$FecInicio[0];

    	$FecFin = explode('/', $value->FecFin);
    	$FecFin = $FecFin[2].'/'.$FecFin[1].'/'.$FecFin[0];

    	echo "INSERT INTO Acuerdos (Codigo, NroAcuerdo, Codigo_Local, C_cliente, Registro_Ac, Membresia, FechaImpresion, Usuario, PC, Estado, Detalle, FechaAnulacion, UsuarioAnulado, PcAnulado, FecContrato, FecInicio, FecFin)
			VALUES (
			".$value->Codigo.", 
			'".$value->NroAcuerdo."', 
			'".$value->Codigo_Local."', 
			".$value->C_cliente.", 
			".$value->Registro_Ac.", 
			'".$value->Membresia."', 
			'".$FechaImpresion2[2].'-'.$FechaImpresion2[1].'-'.$FechaImpresion2[0].' '.$FechaImpresion1[1]."', 
			'".$value->Usuario."', 
			'".$value->PC."', 
			'".$value->Estado."', 
			'".$detalle."', 
			'".$FechaAnulacion."', 
			'".$UsuarioAnulado."', 
			'".$PcAnulado."', 
			'".$FecContrato."', 
			'".$FecInicio."', 
			'".$FecFin."');<br><br>";
			// die();
    }
});